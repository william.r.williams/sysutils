#!/bin/bash
####################################################################################################
#   security_scan.sh v1.1
#   This script is meant to aid System Security Officers in assuring system compliance
#   -Bill W. 05Dec2016
#   william.r.williams@gmail.com                        
####################################################################################################

# Are we root?
if [ `whoami` != "root" ] ; then
  echo "This script must be run either as root or using sudo."
  exit 1
fi

USAGE="          
  This script is meant to aid System Security Officers in assuring system compliance.
  Requires at least one of following arguments:
          -u             Update McAfee DAT tar file from CD/DVD ROM only
          -s             Execute McAfee AntiVirus Scan
          -r             Run Report on System Users
          -a             Execute All of the Above
"

MYARGS="$@"
if [ -z "$MYARGS" ]; then
  echo -e "\nERROR: This script requires at least one of the following arguments. See Usage Below:\n" | tee -a $LOG
  echo "$USAGE"
  exit 1
fi


TIME="$(date +%d%b%Y_%H%M%S)"
LOG="/var/log/$(basename $0)_${TIME}.log"
UTILS="uvscan aureport"

# uvscan is usually here
if [ -d /opt/mcafee ]; then
  PATH=$PATH:/opt/mcafee
fi

chkutil() {

  for i in $@; do
    which ${i} &> /dev/null
      if [ $? -ne 0 ]; then
        echo "ERROR: ${i} is not installed or not in your path?"
        NOTFOUND=true
      fi
    done

  if [ -n "$NOTFOUND" ]; then
    echo
    echo -e "Cannot continue. One or more utilities this script needs to execute were not found.
PATH is $PATH"
    echo
    exit 1
  fi
}

update_dat_file () {
  # mount cd/dvd drive
  WORM=sr0

  if [ -e /dev/"${WORM}" ] && ! grep -q "${WORM}" /etc/mtab; then
    mount /dev/"${WORM}" /media
  elif grep -q "${WORM}" /etc/mtab; then
    # remount to a place we can easily search
    umount /dev/"${WORM}"
    mount /dev/"${WORM}" /media
  else
    echo "ERROR: Could not locate CD/DVD ROM device ${WORM} to mount" | tee -a $LOG
    exit 1
  fi


  # look for dat file
  DAT="$(ls -t /media/*.tar | head -n1)"

  if [ -z "${DAT}" ]; then
    echo "ERROR: DAT tar file was not found." | tee -a $LOG
    exit 1
  else
    echo -e "McAfee DAT tar file found at ${DAT}\n" | tee -a $LOG
  fi

  # Extract the avvdat tar file to mcafee 
  DIR_MCAFEE_HOME="/opt/mcafee"
  echo "Extracting ${DAT} to $DIR_MCAFEE_HOME..." | tee -a $LOG
  tar xvf ${DAT} -C $DIR_MCAFEE_HOME  2>>$LOG

  # Show Version of what avvdat definition was installed
  ${UVSCAN} --version | grep "Dat set version" | tee -a $LOG

  umount /dev/"${WORM}"

}

run_uvscan () {
  EXCLUDELIST="/data"
  for d in $EXCLUDELIST; do
    if [ -e "${d}" ] && ! grep -q "${d}" /opt/mcafee/excludes; then
      echo "${d}" >> /opt/mcafee/excludes
    fi
  done

  if [ ! -d /QUARANTINE ]; then
    mkdir /QUARANTINE
  fi

  # Start Virus Scan
  echo Starting virus scan at `date` | tee -a $LOG
  $UVSCAN -rpc --mime --program -m /QUARANTINE --ignore-links --secure --summary --exclude /opt/mcafee/excludes - / | tee -a $LOG

}

generate_user_report () {
  echo -e "\nSuccessful Account Login History" | tee -a $LOG
  echo '===========================' | tee -a $LOG
  last | tee -a $LOG
  echo | tee -a $LOG
  # Full output of aureport utility
  aureport | tee -a $LOG
  # Summary of failed logins
  echo '===========================' | tee -a $LOG
  aureport -u --failed --summary -i | tee -a $LOG
  echo '===========================' | tee -a $LOG
  # Full details of all failed logins
  aureport -au --failed | tee -a $LOG
  echo | tee -a $LOG

}


## Main

chkutil "${UTILS}"

OPTIND=1

echo -e "\nExecuting $0 on $HOSTNAME at ${TIME}\n" | tee -a $LOG
echo -e "\nLog file is ${LOG}\n\n" | tee -a $LOG

while getopts "usra" opt; do
  case  "$opt" in
     u)
       chk_4_uvscan
       update_dat_file
     ;;

     s)
       chk_4_uvscan
       run_uvscan
     ;;

     r)
       generate_user_report
     ;;

     a)
       chk_4_uvscan
       update_dat_file
       run_uvscan
       generate_user_report
     ;;

     *)
       echo "$USAGE"
       rm -f $LOG
     ;;

  esac
done

shift $((OPTIND-1))
[ "$1" = "--" ] && shift
