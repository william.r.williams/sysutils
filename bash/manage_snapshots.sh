#!/bin/bash
####################################################################################################
#   manage_snapshots.sh
#   The purpose of the script is to create and restore 'full snaphots' - LVM thin-provisioned snapshots
#   along with a backup of the boot partition and rpm database if on a separate volume or partition.
#   -Bill W. 12Apr2017
#   william.r.williams@gmail.com                        
####################################################################################################

MYARGS="$@"
# Snapshot files will be kept at the location below
SNAPBAKDIR="/root"
# Important! Make certain to change this whatever your boot partition is
BOOTPART=sda1

Usage () {

USAGE="          
  This script is meant to is to create and restore 'full snaphots' - LVM thin-provisioned snapshots
  of the root file system along with a backup of the boot partition and rpm database if on a
  separate volume or partition. Requires at least one of following arguments:
    -c                   Create snapshot
    -r                   Restore snapshot
"

echo "${USAGE}"

}


preExecChk () {

  # Are We Root?
  if [ `whoami` != "root" ] ; then
    echo "This script must be run either as root or using sudo."
    exit 1
  fi

  # Is the System Storage Manager utility present?
  if [ ! -e /usr/bin/ssm ]; then
    echo "ERROR: Please make sure the system-storage-manager rpm package is installed."
    exit 1
  fi

  # Does the root file system reside on a thin-provisioned LVM volume?
  ! ssm list vol | grep LV_root | grep -q thin && echo "ERROR: No thin-provisioned LV_root volume?" && exit 1

}


snapshot () {

  ROOTSNAP="$(ssm list snap | grep rootfs_orig_snapshot)"
  VARMNT="$(mount -l | grep '/var ')"

  pushd "${SNAPBAKDIR}" >/dev/null
  if [ "$1" == "create" ]; then
    # Is there an existing original rootfs snapshot?
    if [ -n "${ROOTSNAP}" ]; then
      echo "Already an existing original rootfs snapshot? Exiting Now..."
      exit 1
    else 
      echo "Creating snapshot..."
    fi
    echo "Backing up the boot partition..."
    </dev/${BOOTPART} gzip -c > ${BOOTPART}_orig.img.gz
    # if /var is mounted on a separate volume or partition create an archive of the rpm database
    if [ -n "${VARMNT}" ]; then
      echo "Backing up the RPM database..."
      tar czPf rpmdb_orig.tgz /var/lib/rpm/
    fi
    # take a shapshot of the root fs
    echo "Creating root filesystem snapshot..."
    ssm snapshot /dev/VolGroup00/LV_root -n rootfs_orig_snapshot
    echo "Success"'!'
  elif [ "$1" == "restore" ]; then
    if [ -n "${ROOTSNAP}" ] && [ -e ${BOOTPART}_orig.img.gz ]; then
      while true; do
        read -p "After snapshot is restored this system will be rebooted. Continue? (y/n): " yn
        case $yn in
          [Yy]* ) echo "Restoring snapshot..."
                  # first restore the boot partition
                  echo "Restoring boot partition..."
                  umount /dev/${BOOTPART}
                  <${BOOTPART}_orig.img.gz gzip -dc >/dev/${BOOTPART}
                  # now restore the snapshot
                  echo "Merging original root filesytem snapshot..."
                  lvconvert --merge /dev/VolGroup00/rootfs_orig_snapshot
                  # is there a backup of the rpm db? If so restore that..
                  [ -e rpmdb_orig.tgz ] && echo "Restoring RPM database..." && tar zxPf rpmdb_orig.tgz
                  # reboot is needed to complete root fs snapshot restore
                  reboot
                  break;;
          [Nn]* ) echo "Skipping snapshot restore..."
                  echo
                  break;;
              * ) echo "Please answer yes or no.";;
        esac
      done
    else
      echo "ERROR: original rootfs snapshot and/or boot partition backup not found."
      exit 1
    fi
  else
    echo "Unknown Action."
    exit 1
  fi
  popd >/dev/null

}



### Main ###

# What do we want to do?
if [ "${MYARGS}" == "-c" ]; then
  SNAPACTION=create
elif [ "${MYARGS}" == "-r" ]; then
  SNAPACTION=restore
else
  Usage
  exit 1
fi

# Can we do?
preExecChk

# Let's do
snapshot "${SNAPACTION}"
