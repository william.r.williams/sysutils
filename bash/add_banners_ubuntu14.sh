#!/bin/bash

###########################################################
#   Bash script to enable US Govt Security Banners        #
#   on Ubuntu 14 systems. -Bill W.                        #
#   william.r.williams@gmail.com                          #
###########################################################

# Are we root?
if [ `whoami` != "root" ] ; then
  echo "This script must be run either as root or using sudo."
  exit 1
fi

# confirm this system is Ubuntu 14 otherwise exit script
! python -mplatform | grep -q "Ubuntu-14" && echo "This is not an Ubuntu 14 system" && exit 1

for f in /etc/issue.net /etc/motd; do
  if [ -e "${f}" ]; then
    mv "${f}" "${f}.bak"
  fi
done

# recreate issue.net
cat <<'USG' > /etc/issue.net
You are accessing a U.S. Government (USG) information
system (IS) that is provided for USG-authorized use only.

By using this IS(which includes any device attached to this IS),
you consent to the following conditions:

-The USG routinely intercepts and monitors communications on
this IS for purposes including, but not limited to, penetration
testing, COMSEC monitoring, network operations and defense,
personnel misconduct (PM), law enforcement (LE), and
counterintelligence (CI) investigations.
-At any time, the USG may inspect and seize data stored on this IS.
-Communications using, or data stored on, this IS are not
private, are subject to routine monitoring, interception, and
search, and may be disclosed or used for any USG authorized purpose.
-This IS includes security measures (e.g., authentication and
access controls) to protect USG interests--not for your personal
benefit or privacy.
-Not withstanding the above, using this IS does not constitute
consent to PM, LE or CI investigative searching or monitoring of
the content of privileged communications, or work product, related
to personal representation or services by attorneys,
psychotherapists, or clergy, and their assistants. Such
communications and work product are private and confidential. See
User Agreement for details.
USG

# Message Of The Day becomes a symlink
pushd /etc/ > /dev/null
ln -s issue.net motd
popd > /dev/null

# Enable banner for ssh
sed -e 's/^#Banner /Banner /' -i /etc/ssh/sshd_config

# If there's a graphical environment (e.g. lightdm)...
LDMF="/usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf"
GMSG="session-setup-script=sh -c 'DISPLAY=\":0.0\" gdialog --textbox /etc/issue.net 88 67'"
GFILE="/usr/share/xgreeters/unity-greeter.desktop"
GBSCRIPT="/usr/local/bin/greeter-banner.sh"

# greeter banner before login
if ! grep -q "greeter-banner.sh" "${GFILE}"; then
  sed -e 's/Exec.*$/Exec=\/usr\/local\/bin\/greeter-banner.sh/' -i "${GFILE}"
  cat <<'GB' > ${GBSCRIPT}
#!/bin/bash
false
while [ $? -ne 0 ]; do
  sleep 1
  /usr/bin/gdialog --textbox /etc/issue.net 99 80
done
unity-greeter
GB
  chmod 755 ${GBSCRIPT}
fi

# enable an after-login banner as well
if [ -e "${LDMF}" ] && ! grep -q gdialog "${LDMF}"; then
 echo "${GMSG}" >> "${LDMF}"
fi

