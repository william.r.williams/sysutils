#!/bin/bash

UTILS="pgrep taskset"
DAEMONS="/usr/sbin/cupsd nginx"

chkutil() {

  for i in $@; do
    which ${i} &> /dev/null
      if [ $? -ne 0 ]; then
        echo "ERROR: ${i} is not installed or not in your path?"
        NOTFOUND=true
      fi
    done

  if [ -n "$NOTFOUND" ]; then
    echo
    echo -e "Cannot continue. One or more utilities this script needs to execute were not found.
PATH is $PATH"
    echo
    exit 1
  fi
}

set_to_cpu0() {
  for d in $@; do
    taskset -pc 0 $(pgrep -f ${d}) &> /dev/null
  done
}

## Main

chkutil "${UTILS}"
set_to_cpu0 "${DAEMONS}"

