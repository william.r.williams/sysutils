#!/bin/bash
####################################################################################################
#   rename_rhel7_interfaces.sh
#   This script renames network interfaces on RedHat 7 platforms leveraging existing udev rules - see
#   https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Networking_Guide/\
#   sec-Understanding_the_Device_Renaming_Procedure.html
#   Also works on CentOS 7.
#   -Bill W. 28Mar2017
#   william.r.williams@gmail.com                        
####################################################################################################

NSCRIPTSDIR="/etc/sysconfig/network-scripts"


preExecChk () {

  # We Got Root?
  if [ `whoami` != "root" ] ; then
    echo "This script must be run either as root or using sudo."
    exit 1
  fi
  
  # Platform Version Supported?
  RHVERFILE=/etc/redhat-release
  SPV1='Red Hat Enterprise Linux Server release 7'
  SPV2='CentOS Linux release 7'
  if ! grep -qE "${SPV1}|${SPV2}" ${RHVERFILE} &>/dev/null; then 
    echo "ERROR: This script only supports ${SPV1} and ${SPV2}."
    exit 1
  fi
  
  # Udev Renaming Rules Present?
  if ! [ -e /usr/lib/udev/rules.d/60-net.rules ] || ! [ -e /lib/udev/rename_device ] ; then
    echo "ERROR: RHEL7/CentOS7 udev renaming rules do not seem to present?? Not proceeding..."
    exit 1
  fi

}


backup_restore_ifcfg_files () {

  if [ "${1}" == "backup" ]; then
    BAKDIR="$(basename $0)"_"$(date +%d%b%Y_%H%M%S)"_bak
    echo "backing up ifcfg-* files to ${NSCRIPTSDIR}/${BAKDIR}/"
    mkdir "${NSCRIPTSDIR}/${BAKDIR}/"
    cp -p "${NSCRIPTSDIR}"/ifcfg-* "${NSCRIPTSDIR}/${BAKDIR}/"
  elif [ "${1}" == "restore" ]; then
    # use most recent restore directory
    RESTOREDIR="$(ls -dt ${NSCRIPTSDIR}/$(basename $0)_*_bak | head -1)"
    if [ -d "${RESTOREDIR}" ] && stat "${RESTOREDIR}"/ifcfg-* &>/dev/null; then
      echo "Restoring ifcfg-* files from ${RESTOREDIR}/..."
      rm -f "${NSCRIPTSDIR}"/ifcfg-*
      cp -pf "${RESTOREDIR}"/ifcfg-* "${NSCRIPTSDIR}"/
      echo "Done. Please reboot to take effect."
      exit 0
    else echo "ERROR: Restore Failed, backup of ifcfg-* files was not found."
      exit 1
    fi
  fi
}


rename_ints () {

  echo "Renaming ${1} to ${2}..."

  NEWINTCFG=${2}.TMP000

   if [ -e "${NSCRIPTSDIR}/ifcfg-${1}" ]; then
     mv "${NSCRIPTSDIR}/ifcfg-${1}" "${NSCRIPTSDIR}/ifcfg-${NEWINTCFG}"
     sed -e "s/${1}/${2}/g" -i "${NSCRIPTSDIR}/ifcfg-${NEWINTCFG}"
   else
     cat <<"NEWCFG" > "${NSCRIPTSDIR}/ifcfg-${NEWINTCFG}"
NAME=
DEVICE=
ONBOOT=yes
NETBOOT=yes
BOOTPROTO=dhcp
TYPE=Ethernet
NEWCFG
   sed -e "s/NAME=/NAME=${2}/" -e "s/DEVICE=/DEVICE=${2}/" -i "${NSCRIPTSDIR}/ifcfg-${NEWINTCFG}"
   fi

  # make sure default interface's mac address is included its new network cfg file else renaming won't work
   if ! grep -q "$3" "${NSCRIPTSDIR}/ifcfg-${NEWINTCFG}"; then
     sed 's/HWADDR=/#HWADDR=/' -i "${NSCRIPTSDIR}/ifcfg-${NEWINTCFG}"
     echo "HWADDR=$3" >> "${NSCRIPTSDIR}/ifcfg-${NEWINTCFG}"
   fi

}



###+++ Main +++###

preExecChk

# backup or restore network config files. '-n' arg means skip backup..
if [ "${1}" == "-n" ]; then
  :
elif [ "${1}" == "-r" ]; then
  backup_restore_ifcfg_files restore
else
  backup_restore_ifcfg_files backup
fi


# Create an array of interfaces by looking under /proc
mapfile -t INTERFACES < <(grep -Ev 'Inter|face|lo' /proc/net/dev | awk '{print $1}' | sed -e 's/://')

# Create a hash of ints -> mac addrs, will use the above array
declare -A INTSMACS
for i in ${INTERFACES[@]}; do
  INTMAC="$(ifconfig ${i} | grep ether | awk '{print $2}')"
  INTSMACS+=(["${i}"]="${INTMAC}")
done

# Iterate through list of interfaces
echo -e "\nInterfaces on this system are as follows:\n"
for int in "${!INTSMACS[@]}"; do
  echo "${int} -> ${INTSMACS[$int]}"
done
echo

# Ask to rename
for int in "${!INTSMACS[@]}"; do
  while true; do
    read -p "Rename ${int}? (y/n): " yn
    case $yn in
      [Yy]* ) read -p "New name for ${int}? " INTNEWNAME
              rename_ints ${int} ${INTNEWNAME} ${INTSMACS[$int]}
              echo
              break;;
      [Nn]* ) echo "Skipping rename of ${int}..."
              echo
              break;;
          * ) echo "Please answer yes or no.";;
    esac
  done
done

# if anything was done remove temporary extensions on interface files
if stat "${NSCRIPTSDIR}"/*.TMP000 &>/dev/null; then
  pushd ${NSCRIPTSDIR} >/dev/null
  for f in *.TMP000; do mv $f $(echo $f | sed "s/\.TMP000//"); done
  popd >/dev/null
  echo "Please reboot to take effect."
fi

