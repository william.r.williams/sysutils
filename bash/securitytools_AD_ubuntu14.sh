#!/bin/bash

# Are we root?
if [ `whoami` != "root" ] ; then
  echo "This script must be run either as root or using sudo."
  exit 1
fi

HTTPSVR="http://spacewalk/ubuntu14/"
IEM="BESAgent-9.2.1.48-ubuntu10.amd64.deb"
IEMCFG="actionsite.afxm"
ENCASE64="enlinuxpc-ubuntu64.tar"
MCAFEE="mcafee64.tar"

# check to see if we have at least on argument
USAGE="          
  This script is meant to aid Group 99 system administrators in adding an Ubuntu 14 system
  to OUR AD and installing the requisite security tools. Requires at least one of following arguments:
          -i                   Install and configure IBM Endpoint Manager
          -m                   Install and configure McAfee Anti Virus
          -e                   Install and configure Encase Forensics Software
          -a <adm acct>        Set up AD & use <adm acct>
"

MYARGS="$@"
if [ -z "$MYARGS" ]; then
  echo "$USAGE"
  exit 1
fi


# function add a ubuntu 14 system to OUR Active Directory
u14_our_ad () {
  echo "Setting up OUR Active Directory with $1 as admin account..."

  # Set up http system proxy for aptitude, install prerequisite packages
  gsettings set org.gnome.system.proxy mode 'manual'
  gsettings set org.gnome.system.proxy.http host 'www/proxy.pac'
  gsettings set org.gnome.system.proxy.http port 8080
  apt-get update

  apt-get -y install aptitude ntp ntpdate winbind samba libnss-winbind libpam-winbind krb5-config krb5-locales krb5-user cifs-utils
  aptitude purge libnss-mdns
  # back up and edit configuration files - first go to /etc/
  pushd /etc/ > /dev/null
  BACKUPDIR="/root/backup_config_files_$(date +%d%b%Y |tr [:upper:] [:lower:])"
  mkdir $BACKUPDIR

# need to re-create a number of config files - set aside the originals
# krb5.conf
  if [ -e krb5.conf ]; then mv krb5.conf ${BACKUPDIR}; fi
  cat <<'KRB5' > krb5.conf
[libdefaults]
	#default_realm = ATHENA.MIT.EDU
        ticket_lifetime = 604800
        default_realm = OUR.AD.LOCAL
        default_tgs_entypes = rc4-hmac des-cbc-md5
        default_tkt__enctypes = rc4-hmac des-cbc-md5
        permitted_enctypes = rc4-hmac des-cbc-md5
        dns_lookup_realm = true
        dns_lookup_kdc = true
        dns_fallback = yes


# The following krb5.conf variables are only for MIT Kerberos.
	krb4_config = /etc/krb.conf
	krb4_realms = /etc/krb.realms
	kdc_timesync = 1
	ccache_type = 4
	forwardable = true
	proxiable = true

# The following encryption type specification will be used by MIT Kerberos
# if uncommented.  In general, the defaults in the MIT Kerberos code are
# correct and overriding these specifications only serves to disable new
# encryption types as they are added, creating interoperability problems.
#
# Thie only time when you might need to uncomment these lines and change
# the enctypes is if you have local software that will break on ticket
# caches containing ticket encryption types it doesn't know about (such as
# old versions of Sun Java).

#	default_tgs_enctypes = des3-hmac-sha1
#	default_tkt_enctypes = des3-hmac-sha1
#	permitted_enctypes = des3-hmac-sha1

# The following libdefaults parameters are only for Heimdal Kerberos.
	v4_instance_resolve = false
	v4_name_convert = {
		host = {
			rcmd = host
			ftp = ftp
		}
		plain = {
			something = something-else
		}
	}
	fcc-mit-ticketflags = true

[realms]
    OUR.AD.LOCAL = {
    kdc = lladdc
    default_domain = our.ad.local
    }
	ATHENA.MIT.EDU = {
		kdc = kerberos.our.com:88
		kdc = kerberos-1.our.com:88
		kdc = kerberos-2.our.com:88
		admin_server = kerberos.our.com
		default_domain = our.com
	}
	MEDIA-LAB.MIT.EDU = {
		kdc = kerberos.media.our.com
		admin_server = kerberos.media.our.com
	}
	ZONE.MIT.EDU = {
		kdc = casio.our.com
		kdc = seiko.our.com
		admin_server = casio.our.com
	}
	MOOF.MIT.EDU = {
		kdc = three-headed-dogcow.our.com:88
		kdc = three-headed-dogcow-1.our.com:88
		admin_server = three-headed-dogcow.our.com
	}
	CSAIL.MIT.EDU = {
		kdc = kerberos-1.csail.our.com
		kdc = kerberos-2.csail.our.com
		admin_server = kerberos.csail.our.com
		default_domain = csail.our.com
		krb524_server = krb524.csail.our.com
	}
	IHTFP.ORG = {
		kdc = kerberos.ihtfp.org
		admin_server = kerberos.ihtfp.org
	}
	GNU.ORG = {
		kdc = kerberos.gnu.org
		kdc = kerberos-2.gnu.org
		kdc = kerberos-3.gnu.org
		admin_server = kerberos.gnu.org
	}
	1TS.ORG = {
		kdc = kerberos.1ts.org
		admin_server = kerberos.1ts.org
	}
	GRATUITOUS.ORG = {
		kdc = kerberos.gratuitous.org
		admin_server = kerberos.gratuitous.org
	}
	DOOMCOM.ORG = {
		kdc = kerberos.doomcom.org
		admin_server = kerberos.doomcom.org
	}
	ANDREW.CMU.EDU = {
		kdc = kerberos.andrew.cmu.edu
		kdc = kerberos2.andrew.cmu.edu
		kdc = kerberos3.andrew.cmu.edu
		admin_server = kerberos.andrew.cmu.edu
		default_domain = andrew.cmu.edu
	}
	CS.CMU.EDU = {
		kdc = kerberos.cs.cmu.edu
		kdc = kerberos-2.srv.cs.cmu.edu
		admin_server = kerberos.cs.cmu.edu
	}
	DEMENTIA.ORG = {
		kdc = kerberos.dementix.org
		kdc = kerberos2.dementix.org
		admin_server = kerberos.dementix.org
	}
	stanford.edu = {
		kdc = krb5auth1.stanford.edu
		kdc = krb5auth2.stanford.edu
		kdc = krb5auth3.stanford.edu
		master_kdc = krb5auth1.stanford.edu
		admin_server = krb5-admin.stanford.edu
		default_domain = stanford.edu
	}
        UTORONTO.CA = {
                kdc = kerberos1.utoronto.ca
                kdc = kerberos2.utoronto.ca
                kdc = kerberos3.utoronto.ca
                admin_server = kerberos1.utoronto.ca
                default_domain = utoronto.ca
	}

[domain_realm]
	.our.com = ATHENA.MIT.EDU
	our.com = ATHENA.MIT.EDU
	.media.our.com = MEDIA-LAB.MIT.EDU
	media.our.com = MEDIA-LAB.MIT.EDU
	.csail.our.com = CSAIL.MIT.EDU
	csail.our.com = CSAIL.MIT.EDU
	.whoi.edu = ATHENA.MIT.EDU
	whoi.edu = ATHENA.MIT.EDU
	.stanford.edu = stanford.edu
	.slac.stanford.edu = SLAC.STANFORD.EDU
        .toronto.edu = UTORONTO.CA
        .utoronto.ca = UTORONTO.CA

[login]
	krb4_convert = true
	krb4_get_tickets = false

[appdefaults]
    pam = {
    debug = fale
    ticket_lifetime = 604800
    renew_lifetime = 604800
    forwardable = true
    krb4_convert = false
}

[logging]
    default = FILE:/var/log/krb5libs.log
    kdc = FILE:/var/log/krb5kdc.log
    admin_server = FILE:/var/log/kadmind.log
KRB5

  # nsswitch.conf
  if [ -e nsswitch.conf ]; then mv nsswitch.conf ${BACKUPDIR}; fi
  cat <<'NSS' > nsswitch.conf
# /etc/nsswitch.conf
#
# Example configuration of GNU Name Service Switch functionality.
# If you have the `glibc-doc-reference' and `info' packages installed, try:
# `info libc "Name Service Switch"' for information about this file.

passwd:         compat winbind
group:          compat winbind
shadow:         compat

hosts:          files dns
networks:       files

protocols:      db files
services:       db files
ethers:         db files
rpc:            db files

netgroup:       nis
NSS

# ntp.conf
if [ -e ntp.conf ]; then mv ntp.conf ${BACKUPDIR}; fi
cat <<'NTP' > ntp.conf
# /etc/ntp.conf, configuration for ntpd; see ntp.conf(5) for help

driftfile /var/lib/ntp/ntp.drift


# Enable this if you want statistics to be logged.
#statsdir /var/log/ntpstats/

statistics loopstats peerstats clockstats
filegen loopstats file loopstats type day enable
filegen peerstats file peerstats type day enable
filegen clockstats file clockstats type day enable

# Specify one or more NTP servers.

# Use servers from the NTP Pool Project. Approved by Ubuntu Technical Board
# on 2011-02-08 (LP: #104525). See http://www.pool.ntp.org/join.html for
# more information.
#server 0.ubuntu.pool.ntp.org
#server 1.ubuntu.pool.ntp.org
#server 2.ubuntu.pool.ntp.org
#server 3.ubuntu.pool.ntp.org
server ntp1.lan.com
server ntp2.lan.com
server ntp3.lan.com

# Use Ubuntu's ntp server as a fallback.
#server ntp.ubuntu.com

# Access control configuration; see /usr/share/doc/ntp-doc/html/accopt.html for
# details.  The web page <http://support.ntp.org/bin/view/Support/AccessRestrictions>
# might also be helpful.
#
# Note that "restrict" applies to both servers and clients, so a configuration
# that might be intended to block requests from certain clients could also end
# up blocking replies from your own upstream servers.

# By default, exchange time with everybody, but don't allow configuration.
restrict -4 default kod notrap nomodify nopeer noquery
restrict -6 default kod notrap nomodify nopeer noquery

# Local users may interrogate the ntp server more closely.
restrict 127.0.0.1
restrict ::1

# Clients from this (example!) subnet have unlimited access, but only if
# cryptographically authenticated.
#restrict 192.168.123.0 mask 255.255.255.0 notrust


# If you want to provide time to your local subnet, change the next line.
# (Again, the address is an example only.)
#broadcast 192.168.123.255

# If you want to listen to time broadcasts on your local subnet, de-comment the
# next lines.  Please do this only if you trust everybody on the network!
#disable auth
#broadcastclient
NTP

  # done under /etc/
  popd > /dev/null

  # time needs to be in sync for AD/kerberos keys et al
  /etc/init.d/ntp stop; ntpdate ntp1.lan.our.com; /etc/init.d/ntp start

  # smb.conf - now go to /etc/samba/
  pushd /etc/samba/ > /dev/null
  if [ -e smb.conf ]; then mv smb.conf ${BACKUPDIR}; fi
  cat <<'SMB' > smb.conf
#
# Sample configuration file for the Samba suite for Debian GNU/Linux.
#
#
# This is the main Samba configuration file. You should read the
# smb.conf(5) manual page in order to understand the options listed
# here. Samba has a huge number of configurable options most of which 
# are not shown in this example
#
# Some options that are often worth tuning have been included as
# commented-out examples in this file.
#  - When such options are commented with ";", the proposed setting
#    differs from the default Samba behaviour
#  - When commented with "#", the proposed setting is the default
#    behaviour of Samba but the option is considered important
#    enough to be mentioned here
#
# NOTE: Whenever you modify this file you should run the command
# "testparm" to check that you have not made any basic syntactic 
# errors. 

#======================= Global Settings =======================

[global]
    workgroup = OUR
    security = ADS
    realm = OUR.AD.LOCAL
    preferred master = no
    encrypt passwords = yes
    idmap config *:backend = rid
    idmap config *:range = 5000-200000
    allow trusted domains = yes
    winbind trusted domains only = no
    winbind use default domain = yes
    winbind enum users  = yes
    winbind enum groups = yes
    winbind nested groups = yes
    winbind separator = +
    winbind refresh tickets = yes
    winbind offline logon = yes

    server signing = mandatory
    client signing = mandatory
    client use spnego = yes
    netbios name =
    name resolve order = wins bcast lmhosts host
    ntlm auth = yes
    lanman auth = no
    client ntlmv2 auth = yes

    template shell = /bin/bash
    template homedir = /home/%U

#### Debugging/Accounting ####
# This is the log level

    log level = 3

    load printers = no
    printcap name = /dev/null
    disable spoolss = yes

## Browsing/Identification ###

# Change this to the workgroup/NT-domain name your Samba server will part of
   #workgroup = WORKGROUP

# server string is the equivalent of the NT Description field
	server string = %h server (Samba, Ubuntu)

# Windows Internet Name Serving Support Section:
# WINS Support - Tells the NMBD component of Samba to enable its WINS Server
#   wins support = no

# WINS Server - Tells the NMBD components of Samba to be a WINS Client
# Note: Samba can be either a WINS Server, or a WINS Client, but NOT both
;   wins server = w.x.y.z

# This will prevent nmbd to search for NetBIOS names through DNS.
   dns proxy = no

#### Networking ####

# The specific set of interfaces / networks to bind to
# This can be either the interface name or an IP address/netmask;
# interface names are normally preferred
;   interfaces = 127.0.0.0/8 eth0

# Only bind to the named interfaces and/or networks; you must use the
# 'interfaces' option above to use this.
# It is recommended that you enable this feature if your Samba machine is
# not protected by a firewall or is a firewall itself.  However, this
# option cannot handle dynamic or non-broadcast interfaces correctly.
;   bind interfaces only = yes



#### Debugging/Accounting ####

# This tells Samba to use a separate log file for each machine
# that connects
   log file = /var/log/samba/log.%m

# Cap the size of the individual log files (in KiB).
   max log size = 1000

# If you want Samba to only log through syslog then set the following
# parameter to 'yes'.
#   syslog only = no

# We want Samba to log a minimum amount of information to syslog. Everything
# should go to /var/log/samba/log.{smbd,nmbd} instead. If you want to log
# through syslog you should set the following parameter to something higher.
   syslog = 0

# Do something sensible when Samba crashes: mail the admin a backtrace
   panic action = /usr/share/samba/panic-action %d


####### Authentication #######

# Server role. Defines in which mode Samba will operate. Possible
# values are "standalone server", "member server", "classic primary
# domain controller", "classic backup domain controller", "active
# directory domain controller". 
#
# Most people will want "standalone sever" or "member server".
# Running as "active directory domain controller" will require first
# running "samba-tool domain provision" to wipe databases and create a
# new domain.
   server role = standalone server

# If you are using encrypted passwords, Samba will need to know what
# password database type you are using.  
   passdb backend = tdbsam

   obey pam restrictions = yes

# This boolean parameter controls whether Samba attempts to sync the Unix
# password with the SMB password when the encrypted SMB password in the
# passdb is changed.
   unix password sync = yes

# For Unix password sync to work on a Debian GNU/Linux system, the following
# parameters must be set (thanks to Ian Kahan <<kahan@informatik.tu-muenchen.de> for
# sending the correct chat script for the passwd program in Debian Sarge).
   passwd program = /usr/bin/passwd %u
   passwd chat = *Enter\snew\s*\spassword:* %n\n *Retype\snew\s*\spassword:* %n\n *password\supdated\ssuccessfully* .

# This boolean controls whether PAM will be used for password changes
# when requested by an SMB client instead of the program listed in
# 'passwd program'. The default is 'no'.
   pam password change = yes

# This option controls how unsuccessful authentication attempts are mapped
# to anonymous connections
   map to guest = bad user

########## Domains ###########

#
# The following settings only takes effect if 'server role = primary
# classic domain controller', 'server role = backup domain controller'
# or 'domain logons' is set 
#

# It specifies the location of the user's
# profile directory from the client point of view) The following
# required a [profiles] share to be setup on the samba server (see
# below)
;   logon path = \\%N\profiles\%U
# Another common choice is storing the profile in the user's home directory
# (this is Samba's default)
#   logon path = \\%N\%U\profile

# The following setting only takes effect if 'domain logons' is set
# It specifies the location of a user's home directory (from the client
# point of view)
;   logon drive = H:
#   logon home = \\%N\%U

# The following setting only takes effect if 'domain logons' is set
# It specifies the script to run during logon. The script must be stored
# in the [netlogon] share
# NOTE: Must be store in 'DOS' file format convention
;   logon script = logon.cmd

# This allows Unix users to be created on the domain controller via the SAMR
# RPC pipe.  The example command creates a user account with a disabled Unix
# password; please adapt to your needs
; add user script = /usr/sbin/adduser --quiet --disabled-password --gecos "" %u

# This allows machine accounts to be created on the domain controller via the 
# SAMR RPC pipe.  
# The following assumes a "machines" group exists on the system
; add machine script  = /usr/sbin/useradd -g machines -c "%u machine account" -d /var/lib/samba -s /bin/false %u

# This allows Unix groups to be created on the domain controller via the SAMR
# RPC pipe.  
; add group script = /usr/sbin/addgroup --force-badname %g

############ Misc ############

# Using the following line enables you to customise your configuration
# on a per machine basis. The %m gets replaced with the netbios name
# of the machine that is connecting
;   include = /home/samba/etc/smb.conf.%m


# Some defaults for winbind (make sure you're not using the ranges
# for something else.)
;   idmap uid = 10000-20000
;   idmap gid = 10000-20000
;   template shell = /bin/bash

# Setup usershare options to enable non-root users to share folders
# with the net usershare command.

# Maximum number of usershare. 0 (default) means that usershare is disabled.
;   usershare max shares = 100

# Allow users who've been granted usershare privileges to create
# public shares, not just authenticated ones
   usershare allow guests = yes

#======================= Share Definitions =======================

# Un-comment the following (and tweak the other settings below to suit)
# to enable the default home directory shares. This will share each
# user's home directory as \\server\username
;[homes]
;   comment = Home Directories
;   browseable = no

# By default, the home directories are exported read-only. Change the
# next parameter to 'no' if you want to be able to write to them.
;   read only = yes

# File creation mask is set to 0700 for security reasons. If you want to
# create files with group=rw permissions, set next parameter to 0775.
;   create mask = 0700

# Directory creation mask is set to 0700 for security reasons. If you want to
# create dirs. with group=rw permissions, set next parameter to 0775.
;   directory mask = 0700

# By default, \\server\username shares can be connected to by anyone
# with access to the samba server.
# Un-comment the following parameter to make sure that only "username"
# can connect to \\server\username
# This might need tweaking when using external authentication schemes
;   valid users = %S

# Un-comment the following and create the netlogon directory for Domain Logons
# (you need to configure Samba to act as a domain controller too.)
;[netlogon]
;   comment = Network Logon Service
;   path = /home/samba/netlogon
;   guest ok = yes
;   read only = yes

# Un-comment the following and create the profiles directory to store
# users profiles (see the "logon path" option above)
# (you need to configure Samba to act as a domain controller too.)
# The path below should be writable by all users so that their
# profile directory may be created the first time they log on
;[profiles]
;   comment = Users profiles
;   path = /home/samba/profiles
;   guest ok = no
;   browseable = no
;   create mask = 0600
;   directory mask = 0700

[printers]
   comment = All Printers
   browseable = no
   path = /var/spool/samba
   printable = yes
   guest ok = no
   read only = yes
   create mask = 0700

# Windows clients look for this share name as a source of downloadable
# printer drivers
[print$]
   comment = Printer Drivers
   path = /var/lib/samba/printers
   browseable = yes
   read only = yes
   guest ok = no
# Uncomment to allow remote administration of Windows print drivers.
# You may need to replace 'lpadmin' with the name of the group your
# admin users are members of.
# Please note that you also need to set appropriate Unix permissions
# to the drivers directory for these users to have write rights in it
;   write list = root, @lpadmin

SMB

  # add system's hostname as netbios name in smb.conf
  sed -e s/"^ .*netbios name.*$"/"    netbios name = $(hostname)"/ -i smb.conf
  # done with samba
  popd > /dev/null

  # All these are PAM files

  pushd /etc/pam.d/ > /dev/null
  if [ -e common-account ]; then mv common-account ${BACKUPDIR}; fi
  # common-account
  cat <<'CMACCT' > common-account
#
# /etc/pam.d/common-account - authorization settings common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of the authorization modules that define
# the central access policy for use on the system.  The default is to
# only deny service to users whose accounts are expired in /etc/shadow.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.
#

# here are the per-package modules (the "Primary" block)
account	[success=3 new_authtok_reqd=done default=ignore]	pam_unix.so 
account	[success=2 new_authtok_reqd=done default=ignore]	pam_winbind.so 
account	[success=1 default=ignore]	pam_ldap.so 
# here's the fallback if no module succeeds
account	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
account	required			pam_permit.so
# and here are more per-package modules (the "Additional" block)
# end of pam-auth-update config
session required pam_mkhomedir.so skel=/etc/skel   umask=0022
CMACCT

  # common-auth
  if [ -e common-auth ]; then mv common-auth ${BACKUPDIR}; fi
  cat <<'CMAU' > common-auth
#
# /etc/pam.d/common-auth - authentication settings common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of the authentication modules that define
# the central authentication scheme for use on the system
# (e.g., /etc/shadow, LDAP, Kerberos, etc.).  The default is to use the
# traditional Unix authentication mechanisms.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
auth	[success=3 default=ignore]	pam_unix.so nullok_secure
auth	[success=2 default=ignore]	pam_winbind.so krb5_auth krb5_ccache_type=FILE cached_login try_first_pass
auth	[success=1 default=ignore]	pam_ldap.so use_first_pass
# here's the fallback if no module succeeds
auth	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
auth	required			pam_permit.so
# and here are more per-package modules (the "Additional" block)
auth	optional			pam_cap.so 
# end of pam-auth-update config
CMAU

  # common-password
  if [ -e common-password ]; then mv common-password ${BACKUPDIR}; fi
  cat <<'CMP' > common-password
#
# /etc/pam.d/common-password - password-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define the services to be
# used to change user passwords.  The default is pam_unix.

# Explanation of pam_unix options:
#
# The "sha512" option enables salted SHA512 passwords.  Without this option,
# the default is Unix crypt.  Prior releases used the option "md5".
#
# The "obscure" option replaces the old `OBSCURE_CHECKS_ENAB' option in
# login.defs.
#
# See the pam_unix manpage for other options.

# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
password	[success=3 default=ignore]	pam_unix.so obscure sha512
password	[success=2 default=ignore]	pam_winbind.so use_authtok try_first_pass
password	[success=1 user_unknown=ignore default=die]	pam_ldap.so use_authtok try_first_pass
# here's the fallback if no module succeeds
password	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
password	required			pam_permit.so
# and here are more per-package modules (the "Additional" block)
password	optional	pam_gnome_keyring.so 
# end of pam-auth-update config
CMP

  # common-session
  if [ -e common-session ]; then mv common-session ${BACKUPDIR}; fi
  cat <<'CMS' > common-session
#
# /etc/pam.d/common-session - session-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define tasks to be performed
# at the start and end of sessions of *any* kind (both interactive and
# non-interactive).
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
session	[default=1]			pam_permit.so
# here's the fallback if no module succeeds
session	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
session	required			pam_permit.so
# The pam_umask module will set the umask according to the system default in
# /etc/login.defs and user settings, solving the problem of different
# umask settings with different shells, display managers, remote sessions etc.
# See "man pam_umask".
session optional			pam_umask.so
# and here are more per-package modules (the "Additional" block)
session	required	pam_unix.so 
session	optional			pam_winbind.so 
session	optional			pam_ldap.so 
session	optional	pam_systemd.so 
session	optional			pam_ck_connector.so nox11
# end of pam-auth-update config
CMS

  # common-session-noninteractive
  if [ -e common-session-noninteractive ]; then mv common-session-noninteractive ${BACKUPDIR}; fi
  cat <<'CMSNI' > common-session-noninteractive
#
# /etc/pam.d/common-session-noninteractive - session-related modules
# common to all non-interactive services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define tasks to be performed
# at the start and end of all non-interactive sessions.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
session	[default=1]			pam_permit.so
# here's the fallback if no module succeeds
session	requisite			pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
session	required			pam_permit.so
# The pam_umask module will set the umask according to the system default in
# /etc/login.defs and user settings, solving the problem of different
# umask settings with different shells, display managers, remote sessions etc.
# See "man pam_umask".
session optional			pam_umask.so
# and here are more per-package modules (the "Additional" block)
session	required	pam_unix.so 
session	optional			pam_winbind.so 
session	optional			pam_ldap.so 
# end of pam-auth-update config
CMSNI
  popd > /dev/null

  # winbind
  if [ -e /etc/security/pam_winbind.conf ]; then mv /etc/security/pam_winbind.conf ${BACKUPDIR}; fi
  pushd /etc/security/ > /dev/null
  cat <<'WINBIND' > pam_winbind.conf
#
# pam_winbind configuration file
#
# /etc/security/pam_winbind.conf
#

[global]

# turn on debugging
;debug = no

# turn on extended PAM state debugging
;debug_state = no

# request a cached login if possible
# (needs "winbind offline logon = yes" in smb.conf)
cached_login = yes

# authenticate using kerberos
krb5_auth = yes

# when using kerberos, request a "FILE" krb5 credential cache type
# (leave empty to just do krb5 authentication but not have a ticket
# afterwards)
krb5_ccache_type = FILE

# make successful authentication dependend on membership of one SID
# (can also take a name)
;require_membership_of =

# password expiry warning period in days
;warn_pwd_expire = 14

# omit pam conversations
;silent = no

# create homedirectory on the fly
mkhomedir = yes
WINBIND
  popd > /dev/null

  # the next three files fix cached login & winbind mkhomedir permissions issues
  cat <<'FLOGIN' > /etc/profile.d/firstlogin.sh
#!/bin/bash
name=`id -un`
if [ ! -e ~/.fixed-privacy ]
then
 /usr/local/bin/fix-ubuntu-privacy.sh
fi

if [ ! -e ~/.fixed-perms ]
then
 sudo /usr/local/bin/fix-perms.sh $name
fi
FLOGIN
  chmod 755 /etc/profile.d/firstlogin.sh

  cat <<'FIXPRIV' > /usr/local/bin/fix-ubuntu-privacy.sh
#!/bin/bash

GS="/usr/bin/gsettings"
CCUL="com.canonical.Unity.lenses"

# Figure out the version of Ubuntu that you're running
V=`/usr/bin/lsb_release -rs`
# The privacy problems started with 12.10, so earlier versions should do nothing
if awk "BEGIN {exit !($V < 12.10 || $V >= 14.10)}"; then
  echo "Good news! This version of Ubuntu is not known to invade your privacy."
else

  # Check Canonical schema is present. Take first match, ignoring case.
  SCHEMA="`$GS list-schemas | grep -i $CCUL | head -1`"
  if [ -z "$SCHEMA" ]
    then
    printf "Error: could not find Canonical schema %s.\n" "$CCUL" 1>&2
    exit 1
  else
    CCUL="$SCHEMA"
  fi

  # Turn off "Remote Search", so search terms in Dash don't get sent to the internet
  $GS set $CCUL remote-content-search none

  # If you're using earlier than 13.10, uninstall unity-lens-shopping
  if [ $V \< 13.10 ]; then
    sudo apt-get remove -y unity-lens-shopping

  # If you're using a later version, disable remote scopes
  else
    $GS set $CCUL disabled-scopes \
      "['more_suggestions-amazon.scope', 'more_suggestions-u1ms.scope',
      'more_suggestions-populartracks.scope', 'music-musicstore.scope',
      'more_suggestions-ebay.scope', 'more_suggestions-ubuntushop.scope',
      'more_suggestions-skimlinks.scope']"
  fi;

  # Block connections to Ubuntu's ad server, just in case
  if ! grep -q "127.0.0.1 productsearch.ubuntu.com" /etc/hosts; then
    echo -e "\n127.0.0.1 productsearch.ubuntu.com" | sudo tee -a /etc/hosts >/dev/null
  fi

  touch ~/.fixed-privacy
  chown $(login):AD\+domain\ users ~/.fixed-privacy
  echo "All done. Enjoy your privacy."
fi
FIXPRIV
  chmod 755 /usr/local/bin/fix-ubuntu-privacy.sh

  cat <<'FIXPERMS' > /usr/local/bin/fix-perms.sh
#!/bin/bash
name="$1"
echo "name = $name"

if wbinfo -u | grep -q $name && [ $name != "root" ]
then
badfiles=`find /home/"$name" -type f ! -user $name | wc -l`
baddirs=`find /home/"$name" -type d ! -user $name | wc -l`
badlinks=`find /home/"$name" -type l ! -user $name | wc -l`
while [ $badfiles -gt 0 ]
do
  echo Fixing file ownership
  find /home/"$name" -type f ! -user $name -exec chown "$name":AD\+\domain\ users {} \;
  badfiles=`find /home/"$name" -type f ! -user $name | wc -l`
done
while [ $baddirs -gt 0 ]
do
  echo Fixing directory ownership
  find /home/"$name" -type d ! -user $name -exec chown "$name":AD\+\domain\ users {} \;
  baddirs=`find /home/"$name" -type d ! -user $name | wc -l`
done
while [ $badlinks -gt 0 ]
do
  echo Fixing link ownership
  find /home/"$name" -type l ! -user $name -exec chown -h "$name":AD\+\domain\ users {} \;
  badlinks=`find /home/"$name" -type l ! -user $name | wc -l`
done
echo "Done fixing ownership"
  touch /home/"$name"/.fixed-perms
  chown "$name":AD\+domain\ users /home/"$name"/.fixed-perms
fi
FIXPERMS
  chmod 755 /usr/local/bin/fix-perms.sh

  # lightdm login greeter setup
  pushd /usr/share/lightdm/lightdm.conf.d/ > /dev/null
  if [ -e 50-ubuntu.conf ]; then mv 50-ubuntu.conf ${BACKUPDIR}; fi
  cat <<'U50' > 50-ubuntu.conf
[SeatDefaults]
user-session=ubuntu
# see https://wiki.ubuntu.com/LightDM
allow-guest=false
greeter-hide-users=true
greeter-show-manual-login=true
U50
  # finished with the here files
  popd > /dev/null

  # create kerberos credentials
  kinit ${1}@OUR.AD.LOCAL
  # join the domain based on our kerberos authentication above
  net ads join -k

  # Add admins to sudoers
  ! grep -q ^%0123-Administrators /etc/sudoers && echo '%0123-Administrators    ALL=(ALL)       NOPASSWD: ALL' >> /etc/sudoers
  ! grep -q ^%0123-IT /etc/sudoers && echo '%0123-IT    ALL=(ALL)       NOPASSWD: ALL' >> /etc/sudoers

  # restart services
  /etc/init.d/winbind restart; /etc/init.d/nmbd restart; /etc/init.d/smbd restart
  echo "OUR Active Directory configuration is complete. Please reboot to finalize setup. "
}

OPTIND=1

while getopts "iema:" opt; do
  case  "$opt" in
     i)
      # Install IEM
      if ! ( [ -d /opt/BESClient ] || [ -e /etc/init.d/besclient ] ); then
        if curl -sSf ${HTTPSVR}${IEM} &> /dev/null; then
          mkdir -p /etc/opt/BESClient
          pushd /etc/opt/BESClient > /dev/null
          wget ${HTTPSVR}${IEMCFG}
          popd > /dev/null
          pushd /root/ > /dev/null
          wget ${HTTPSVR}${IEM}
          dpkg -i ${IEM}
          /etc/init.d/besclient start
          popd > /dev/null
        else
          echo "ERROR: ${HTTPSVR}${IEMCFG} not found."
        fi
      else
        echo "IEM appears to already be set up on this system."
      fi
      ;;

     e)
      # Install Encase
      if ! ( [ -e /usr/bin/enlinuxpc64 ] || [ -e /etc/init.d/enlinuxpc ] ); then
        if curl -sSf ${HTTPSVR}${ENCASE64} &> /dev/null; then
          pushd /tmp > /dev/null
          wget ${HTTPSVR}${ENCASE64}
          tar xf enlinuxpc-ubuntu64.tar
          mv usr/bin/enlinuxpc64 /usr/bin/
          chmod 740 /usr/bin/enlinuxpc64
          pushd /usr/bin/ > /dev/null; ln -s enlinuxpc64 enlinuxpc; popd > /dev/null
          mv etc/init.d/enlinuxpc /etc/init.d/
          rm -rf /tmp/usr/
          rm -rf /tmp/etc/
          update-rc.d enlinuxpc defaults 93 10
          popd > /dev/null
        else
          echo "ERROR: ${HTTPSVR}${ENCASE64} not found."
        fi
      else
        echo "Encase appears to already be set up on this system."
      fi
      ;;

     m)
      # Install McAfee
      if ! ( [ -d /opt/mcafee ] || [ -e /bin/uvscanme ] || [ -e /bin/uvgetdat ] ); then
        if curl -sSf ${HTTPSVR}${MCAFEE} &> /dev/null; then
          pushd /tmp > /dev/null
          wget ${HTTPSVR}${MCAFEE}
          tar xf mcafee64.tar
          pushd tmp/mcafee > /dev/null

          # the below taken from ll-install-uvscan (& fixed)
          LOG=~/mcafee-install.log
          # check for root
          if [ `whoami` == "root" ]
          then
            #install uvscan to /opt/mcafee
            ./install-uvscan -y /opt/mcafee | tee -a $LOG;

            #copy a basic excludes file to /opt/mcafee
            cp excludes /opt/mcafee | tee -a $LOG;

            #create a quarantine directory
            mkdir /QUARANTINE | tee -a $LOG;

            #copy uvscanme and uvgetdat to /bin
            cp uvscanme /bin/
            cp uvgetdat /bin

            #back up root's crontab, append mcafee stuff to crontab
            echo "If crontab gets blown up manually run crontab ~/crontab.bak to restore original" >> $LOG;
            crontab -l > ~/crontab.bak | tee -a $LOG;
            crontab -l | grep -v uvscanme | grep -v uvgetdat | grep -v uvdatupdate > /tmp/root.crontab | tee -a $LOG;
            cat crontab.append >> /tmp/root.crontab | tee -a $LOG;
            crontab /tmp/root.crontab | tee -a $LOG;

            echo "install finished. please add any directories you would like to exclude from scanning to /opt/mcafee/excludes (one directory per line, full path) and then execute "uvscanme /".

NOTE: I would highly recommend excluding any samba mounts and nfs mounts." | tee -a $LOG;
          else
            echo "
################################
#please run this script as root#
################################
";
          fi

          popd > /dev/null
          rm -rf /tmp/tmp
          rm -f /tmp/root.crontab
          mv mcafee64.tar /root/
          popd > /dev/null
        else
          echo "ERROR: ${HTTPSVR}${MCAFEE} not found."
        fi
      else
        echo "McAfee appears to already be set up on this system."
      fi
      ;;

    a)
      # OUR AD setup
      if grep 'OUR.AD.LOCAL' /etc/samba/smb.conf &> /dev/null; then
        echo "OUR Active Directory appears to be already set up on this system."
      else
        u14_our_ad $OPTARG
      fi
      ;;

    *)
      echo "$USAGE"
      ;;
      
  esac
done

shift $((OPTIND-1))
[ "$1" = "--" ] && shift
