###########################################################
#   Bash script to disable Unity Global Searches (i.e.    #
#   the Internet) from the Desktop on Ubuntu 14 systems   #
#   -Bill W. (william.r.williams@gmail.com)               #
###########################################################

# Are we root?
if [ `whoami` != "root" ] ; then
  echo "This script must be run either as root or using sudo."
  exit 1
fi

# confirm this system is Ubuntu 14 otherwise exit script
! python -mplatform | grep -q "Ubuntu-14" && echo "This is not an Ubuntu 14 system" && exit 1

cat <<'NOSEARCH' > /etc/xdg/autostart/disable_onlinesearch.desktop
[Desktop Entry]
Name=Disable Search
Exec=/bin/bash -c "gsettings set com.canonical.Unity.Lenses remote-content-search 'none'"
Type=Application
NOSEARCH

# Also remove the Amazon shopping icon from desktop
rm -f /usr/share/applications/ubuntu-amazon-default.desktop
