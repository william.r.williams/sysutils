#!/bin/bash
####################################################################################################
#   ad_2fa_sssd.sh
#   Use your Windows Domain Admin account to configure Active Directory, System Security Services Daemon
#   (sssd), and Two Factor Authentication via smart card. The smart card readers supported are
#   Identiv SCR3500 and HID OmniKey 3121.
#   -Bill W. 09jun2017
#   william.r.williams@gmail.com                        
####################################################################################################

# Are we root?
if [ `whoami` != "root" ] ; then
  echo "This script must be run either as root or using sudo."
  exit 1
fi

# Is this a CentOS 7 system?
C7="CentOS Linux release 7"
# Once this is vetted out on RHEL 7 we'll enable support here as well
#R7="Red Hat Enterprise Linux Server release 7"
if grep -q "$C7" /etc/redhat-release &>/dev/null; then
  :
else
  echo "Only CentOS 7 is supported at this time"
  exit 1
fi 

# check to see if we have at least one argument
USAGE="          
This script is meant to aid Group 99 system administrators in configuring a CentOS 7 system
for OUR AD & two factor authentication (via smart card) by way of System Security Services Daemon.
Requires the following argument:

          -a <adm acct>       (ADM account username) 
"

MYARGS="$@"
if [ -z "$MYARGS" ]; then
  echo "$USAGE"
  exit 1
fi


# function add CentOS 7 system to OUR Active Directory
c7_2fa () {
  echo "Begin setup of OUR AD, SSSD, & Two Factor Auth using $1 as admin account..."

  # Our repo needs to be defined for some of the packages below
  if [ ! -e /etc/yum.repos.d/OURcustom.repo ]; then
    cat <<'OURREPO' > /etc/yum.repos.d/OURcustom.repo
[OURcustom]
gpgcheck=0
name=Our Custom Repo
baseurl=http://OUR-spacewalk/OURcustom
proxy=_none_
OURREPO
  fi

  # also make sure EPEL repositories are installed
  yum -y install epel-release

  # Install any of the prerequisite packages listed below
  REQPKGS="sssd-common-pac sssd-client sssd-ldap
python-sssdconfig sssd-ipa sssd-common sssd sssd-krb5
sssd-libwbclient sssd-proxy sssd-ad sssd-krb5-common
oddjob-mkhomedir oddjob samba samba-common
samba-common-tools samba-common-libs samba-winbind samba-libs
samba-client-libs samba-winbind-clients samba-python
samba-winbind-modules samba-client krb5-workstation ntp
nss-tools pcsc-lite-libs pcsc-lite pam_pkcs11 pcsc-lite-ccid coolkey our-ca-certs our-omni pcsc-tools
"

  read -a reqpkgs <<<$REQPKGS
  declare -a pkgs_need_installed
 
  for i in ${reqpkgs[@]}; do
    rpm -q $i &>/dev/null
    if [ $? -eq 1 ]; then
      pkgs_need_installed+=("$i")
    fi
  done
 
  if [ ${#pkgs_need_installed[@]} -ne 0 ]; then
    echo "Installing the following packages for AD/SSSD/2FA Support..."
    yum -y install ${pkgs_need_installed[@]}
  fi

  # All original configuration files will get backed up here
  BACKUPDIR="/root/$0-backup_config_files_$(date +%d%b%Y |tr [:upper:] [:lower:])"
  mkdir $BACKUPDIR

  pushd /etc/ > /dev/null

# need to re-create a number of config files - set aside the originals
# krb5.conf
  if [ -e krb5.conf ]; then mv krb5.conf ${BACKUPDIR}; fi
  cat <<'KRB5' > krb5.conf
[logging]
    default = FILE:/var/log/krb5libs.log
    kdc = FILE:/var/log/krb5kdc.log
    admin_server = FILE:/var/log/kadmind.log

[libdefaults]
        default_realm = OUR.AD.LOCAL
        dns_lookup_realm = true
        dns_lookup_kdc = true
        kdc_timesync = 0
        rdns = false
        forwardable = yes
        renew_lifetime = 7d
        ticket_lifetime = 9h
KRB5


  # make sure our ntp servers are defined in ntp.conf - ip addrs are added by dhclient
  if [ -e ntp.conf ]; then cp -p ntp.conf ${BACKUPDIR}; fi
  if ! grep -qE ^"server ntp[0-9].lan.com|server 10.0.0" /etc/ntp.conf &>/dev/null; then
    sed -e 's/^server/#server/g' -i ntp.conf
    echo "
# entries for ntp servers
server ntp1.lan.com iburst
server ntp2.lan.com iburst
server ntp3.lan.com iburst
" >> ntp.conf

  fi

  # done under /etc/
  popd > /dev/null

  # time needs to be in sync for AD/kerberos keys et al
   systemctl stop ntpd.service; ntpdate ntp1.lan.com; systemctl start ntpd.service

  # smb.conf - now go to /etc/samba/
  pushd /etc/samba/ > /dev/null
  if [ -e smb.conf ]; then mv smb.conf ${BACKUPDIR}; fi
  cat <<'SMB' > smb.conf
[global]
    workgroup = OUR
    realm = OUR.AD.LOCAL
    security = ADS
    kerberos method = secrets and keytab
    client signing = yes
    client use spnego = yes
    template homedir = /home/%U
    template shell = /bin/bash
    server string = Samba Server
    log file = /var/log/samba/samba.log.%m
    idmap config OUR : backend = ad
    idmap config OUR : schema _ mode = rfc2307
    idmap config OUR : range = 1000-9999999
    idmap config OUR : default = yes
    idmap config * : backend = tdb
    idmap config * : range = 200-999

[homes]
    comment = Home directories
    read only = No
    browseable = No
    create mask = 0640
    directory mask = 0750
SMB

  # done with samba
  popd > /dev/null

  # enable & restart the service
  systemctl enable smb.service
  systemctl restart smb.service

  # create kerberos credentials
  kinit ${1}@OUR.AD.LOCAL
  # join the domain based on our kerberos authentication above
  net ads join -k

  # Configure authentication settings for the system
  authconfig --update --enablesssd --enablesssdauth --enablemkhomedir --enablesmartcard

  # Add admins to sudoers
  ! grep -q ^%0123-Administrators /etc/sudoers && echo '%0123-Administrators    ALL=(ALL)       NOPASSWD: ALL' >> /etc/sudoers
  ! grep -q ^%0123-IT /etc/sudoers && echo '%0123-IT    ALL=(ALL)       NOPASSWD: ALL' >> /etc/sudoers

  # System Security Services Daemon - now go to /etc/sssd/
  pushd /etc/sssd/ > /dev/null
  if [ -e sssd.conf ]; then mv sssd.conf ${BACKUPDIR}; fi
  cat <<'SSSD' > sssd.conf
[sssd]
  domains = our.ad.local
  config_file_version = 2
  services = nss, pam

[domain/our.ad.local]
  ad_domain = our.ad.local
  krb5_realm = OUR.AD.LOCAL
  realmd_tags = manages-system joined-with-samba 
  cache_credentials = True
  id_provider = ad
  krb5_store_password_if_offline = True
  default_shell = /bin/bash
  ldap_id_mapping = True
  use_fully_qualified_names = False
  fallback_homedir = /home/%u
  access_provider = ad
SSSD

  chmod 700 sssd.conf

  # done with System Security Services Daemon
  popd > /dev/null
  
  # set up configuration for pam pkcs11 user mapping
  pushd /etc/pam_pkcs11/ > /dev/null
  if [ -e pam_pkcs11.conf ]; then cp -p pam_pkcs11.conf ${BACKUPDIR}; fi
  sed -e 's/use_mappers.*$/use_mappers = ms;/' -e 's/ignoredomain =.*$/ignoredomain = true;/g' \
  -e 's/domain = \"domain.com\";/#domain = \"domain.com\";/' -i pam_pkcs11.conf


  if [ -e pkcs11_eventmgr.conf ]; then cp -p pkcs11_eventmgr.conf ${BACKUPDIR}; fi
    cat <<'PKCS11EVM' > pkcs11_eventmgr.conf
pkcs11_eventmgr  {

	# Run in background? Implies debug=false if true
	daemon = true;

	# show debug messages?
	debug = false;

	# polling time in seconds
	polling_time = 1;

	# expire time in seconds
	# default = 0 ( no expire )
	expire_time = 0;

	# pkcs11 module to use
	pkcs11_module = libcoolkeypk11.so;

	#
	# list of events and actions

	# Card inserted
	event "card_insert" {
		# what to do if an action fail?
		# ignore  : continue to next action
		# return  : end action sequence
		# quit    : end program
		on_error = ignore;

		# You can enter several, comma-separated action entries
		# they will be executed in turn
		action = "/usr/sbin/gdm-safe-restart", "/etc/pkcs11/lockhelper.sh -lock";
	}

	# Card has been removed
	event "card_remove" {
		on_error = ignore;
		action = "/usr/sbin/gdm-safe-restart", "/etc/pkcs11/lockhelper.sh -deactivate";
	}

	# Too much time card removed
	event "expire_time" {
		on_error = ignore;
		action = /bin/false;
	}
}
PKCS11EVM

   popd > /dev/null
  
  
  # Install Lab Infrastructure Certificates
  pushd /etc/pki/ca-trust/source/anchors/ >/dev/null
  for c in ca.crt CA1.crt "OUR CA-2.pem" "OUR CA-3.pem" "OUR CA-4.pem" "OUR CA-5.pem"
  do
    certutil -A -d /etc/pki/nssdb/ -n "${c}" -t "CT,C,C" -i "${c}"
  done
  popd >/dev/null

  # enable sssd, then restart sssd & sshd
  systemctl enable sssd.service
  systemctl restart sssd.service
  systemctl restart sshd.service

  # fingerprint service does not play well with smartcards
  rpm -e fprintd fprintd-pam &>/dev/null

  
  echo "OUR Active Directory & 2 Factor Authentication Configuration is Complete."

}


### MAIN #####################################

OPTIND=1

while getopts "a:" opt; do
  case  "$opt" in

    a)
      # OUR AD setup
      if grep 'OUR.AD.LOCAL' /etc/samba/smb.conf &> /dev/null; then
        echo "OUR Active Directory appears to be already set up on this system."
      else
        c7_2fa $OPTARG
      fi
      ;;

    *)
      echo "$USAGE"
      ;;
      
  esac
done

shift $((OPTIND-1))
[ "$1" = "--" ] && shift
