#!/bin/bash

###################################################################################################
#   A generic script for checking the process list for a given daemon and port
#   Meant to be used in conjunction with the Nagios NRPE agent
#   -Bill W. dec2015
#   william.r.williams@gmail.com                         
###################################################################################################

# read the options
MYARGS=$(getopt -o hd:p: --long help,daemon:,port: -n $0 -- "$@")
eval set -- "$MYARGS"


# lists args
USAGE="          
  A generic script for checking a daemon through the Nagios NRPE agent.
  Returns 0 for a good check, all other exit values will communicate an error.

  Arguments:
          -h or --help                            Display this help message
          -d or --daemon <daemon_name>            Name of the daemon to verify within the process list
          -p or --port                            Port number that the daemon is running on (optional)

"

# Main starts here
# extract options and their arguments into variables.
while true ; do
    case "$1" in
        -h|--help)
            echo "${USAGE}" ; exit 1 ;;
        -d|--daemon)
            case "$2" in
                "") shift 2 ;;
                *) DAEMON="$2" ; shift 2 ;;
            esac ;;
        -p|--port)
            case "$2" in
                "") shift 2 ;;
                *) PORT="$2" ; shift 2 ;;
            esac ;;
        --) shift ; break ;;
        *) echo "${USAGE}" ; exit 1 ;;
    esac
done

# Error Checking to confirm we have valid arguments
if [ "$MYARGS" = ' --' ]; then
  echo
  echo "We need at least a few arguments"'!'
  echo "${USAGE}"
  exit 1
fi

if [ -n "${PORT}" ]; then
  DAEMON_IN_PROC_PORT="$(ps aux | grep "${DAEMON}" | grep -v grep | grep -v $0 | grep ${PORT})"
else
  DAEMON_IN_PROC="$(ps aux | grep "${DAEMON}" | grep -v grep | grep -v $0 | head -1)"
fi

if [ -z "${PORT}" -a -n "${DAEMON_IN_PROC}" ]; then
  echo "OK: ${DAEMON} found with PID $(awk '{print $2}'<<<"${DAEMON_IN_PROC}")"
elif [ -n "${PORT}" -a -n "${DAEMON_IN_PROC_PORT}" ]; then
  echo "OK: ${DAEMON} on port ${PORT} found with PID $(awk '{print $2}'<<<"${DAEMON_IN_PROC_PORT}")"
elif [ -z "${DAEMON_IN_PROC}" -a -z "${PORT}" ]; then
  echo "ERROR: ${DAEMON} not found in process list"
  exit 1
elif [ -n "${PORT}" -a -z "${DAEMON_IN_PROC_PORT}" ]; then
  echo "ERROR: ${DAEMON} on port ${PORT} not found in process list"
  exit 1
else
  echo "Unknown ERROR"
  exit 1
fi
