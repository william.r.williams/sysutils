#!/bin/bash
####################################################################################################
#   toggle_ups_port_pwr.sh
#   A short bash script to turn power either off or on to a particular port using an SNMP OID string
#   that's executed on a networked APC 7900 Smart PDU. A special Thanks to this guy:
#   http://henrysmac.org/blog/2012/2/16/controlling-an-apc-pdu-from-python-via-pysnmp.html
#   -Bill W. 09Sep2016
#   william.r.williams@gmail.com                        
####################################################################################################

# Just some debugging info:
# the below command gets status, a return of 1 means power on and 2 means off
# snmpget -v 1 -c private $IP 1.3.6.1.4.1.318.1.1.4.4.2.1.3.${P}


# Define the ip address of the PDU
IP=155.34.163.109

# The port/outlet number to toggle
P=7

if ! [[ "$1" == "on" || "$1" == "off" ]]; then
    echo "Please provide an argument of either 'off' or 'on'."
    exit 1
fi


echo -n "Turning outlet number ${P} ${1}..."

if [ "$1" == "on" ]; then
    snmpset -v 1 -c private $IP 1.3.6.1.4.1.318.1.1.4.4.2.1.3.${P} i 1 > /dev/null
elif [ "$1" == "off" ]; then
    snmpset -v 1 -c private $IP 1.3.6.1.4.1.318.1.1.4.4.2.1.3.${P} i 2 > /dev/null
else
    echo "UNKNOWN ERROR"
    exit 1
fi

echo "done."

