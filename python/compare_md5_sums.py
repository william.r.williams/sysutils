#!/usr/bin/python

import os
import sys
import getopt
import re

# recursively list all files as absolute path, compute each's respective md5sum,
# and store this in a dictionary
def find_md5_dups(location):

  md5file_dict = {}

  for root, directories, filenames in os.walk(location):
    for filename in filenames:
      md5file = os.popen("md5sum %s" % os.path.join(root,filename)).read().rstrip()
      (md5sum, filefullpath) = md5file.split(None, 1)
      md5file_dict[str(filefullpath)] = md5sum

  md5file_value_list = sorted(set(md5file_dict.values()))

  print "Checking for files with duplicate md5hash sums..."
  for v in md5file_value_list:
    md5hashvalue = v
    md5hashvaluefoundx = 0
    for k, v in md5file_dict.items():
      if v == md5hashvalue:
        md5hashvaluefoundx += 1
    if md5hashvaluefoundx > 1:
      print "\nDuplicate md5hash sum found (%s) for these files:" % md5hashvalue
      for k, v in md5file_dict.items():
        if v == md5hashvalue:
          print k
      print "\n"




# main
searchdir = "~"
find_md5_dups(searchdir)
