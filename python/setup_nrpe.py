#!/usr/bin/python

####################################################################################################
#   Python utility to set up nrpe on a linux system
#   -Bill W. 29jul2015
#   william.r.williams@gmail.com                         
####################################################################################################

import sys, os, getopt, subprocess, time, imp, re, fileinput, urllib2


def usage():
  print "\nUsage: " + sys.argv[0] + """ [ -h -p -s]
  This script is meant to assist system administrators in the installation and configuration of the
  Nagios Remote Plugin Executor (NRPE) agent on Centos/RHEL 5, 6, & 7 systems.

          -h or --help            Print this help message
          -p or --partitions      List of partitions to monitor for disk usage. Default is root partition only.
                                  If specifying multiple partitions enclose list in quotes (e.g. "/ /data /var")
          -s or --server          The name or ip address of the Nagios server that will be doing the monitoring
                                  NOTE: if none specified default is monitor (10.0.0.10)

  Exit Codes:
           0    All Good
           3    Installation of NRPE RPMs failed
  """
def install_nrpe():

  # What OS do we have?
  if os.path.exists("/etc/redhat-release"):
    os_file = open("/etc/redhat-release", "r")
    os_ver = os_file.readline().rstrip()
    os_file.close
  else:
    print "ERROR: This doesn't appear to be either a CentOS or Redhat system?"
    sys.exit(3)

  # Version of OS determines where we will get our rpms...
  if re.match(r'Red Hat Enterprise Linux Server release 5', os_ver):
    rpm_svr_path = "http://spacewalk/rhel5/"
    rpm_list = [ "nagios-common-2.12-10.el5.x86_64.rpm", "nagios-plugins-1.4.15-2.el5.x86_64.rpm",
                 "nagios-plugins-nrpe-2.14-5.el5.x86_64.rpm", "nrpe-2.14-5.el5.x86_64.rpm",
                 "nagios-plugins-disk-1.4.15-2.el5.x86_64.rpm", "nagios-plugins-load-1.4.15-2.el5.x86_64.rpm",
                 "nagios-plugins-procs-1.4.15-2.el5.x86_64.rpm", "nagios-plugins-users-1.4.15-2.el5.x86_64.rpm" ]
  elif re.match(r'Red Hat Enterprise Linux Server release 6', os_ver):
    print "This OS is not currently supported. Check back soon."
    sys.exit(3)
  elif re.match(r'Red Hat Enterprise Linux Server release 7', os_ver):
    print "This OS is not currently supported. Check back soon."
    sys.exit(3)
  elif re.match(r'CentOS release 5', os_ver):
    print "This OS is not currently supported. Check back soon."
    sys.exit(3)
  elif re.match(r'CentOS release 6', os_ver):
    print "This OS is not currently supported. Check back soon."
    sys.exit(3)
  elif re.match(r'CentOS Linux release 7', os_ver):
    rpm_svr_path = "http://spacewalk/centos7/"
    rpm_list = [ "nagios-common-3.5.1-1.el7.x86_64.rpm", "nagios-plugins-2.0.3-3.el7.x86_64.rpm",
                 "nagios-plugins-nrpe-2.15-7.el7.x86_64.rpm", "nrpe-2.15-7.el7.x86_64.rpm",
                 "nagios-plugins-disk-2.0.3-3.el7.x86_64.rpm", "nagios-plugins-load-2.0.3-3.el7.x86_64.rpm",
                 "nagios-plugins-procs-2.0.3-3.el7.x86_64.rpm", "nagios-plugins-users-2.0.3-3.el7.x86_64.rpm" ]
  else:
    print "ERROR: Could not determine OS for Nagios NRPE rpm install."
    sys.exit(3)

  rpm_install_list = ""
  for r in rpm_list:
    rpm_install_list = rpm_install_list + rpm_svr_path + r + " "

  # OK, install rpms now
  print ("Installing Nagios NRPE rpm packages from %s\nfor %s..." % (rpm_svr_path, os_ver))
  try:
    os.system("rpm -ivh %s" % (rpm_install_list))
  except:
    print "ERROR: Nagios NRPE rpms WERE NOT successfully installed"
    sys.exit(3)

def configure_nrpe(nagios_svr, partitions):

  print "Now configuring Nagios NRPE Agent..."

  # Create config file from original, edit accordingly
  orig_nrpe_cfg = open("/etc/nagios/nrpe.cfg").read()
  my_nrpe_cfg = open("/etc/nagios/nrpe.cfg.new", "wt")


  my_dict = {
    # Nagios checks customized for the environment.
    # The below matches would only likely exist in a fresh nrpe installation else they will fail. & that's fine.
    'allowed_hosts=127.0.0.1' : 'allowed_hosts=127.0.0.1,' + nagios_svr,
    'dont_blame_nrpe=0' : 'dont_blame_nrpe=1',
    'command[check_users]=/usr/lib64/nagios/plugins/check_users -w 5 -c 10' : 'command[check_users]=/usr/lib64/nagios/plugins/check_users -w 10 -c 15',
    'command[check_total_procs]=/usr/lib64/nagios/plugins/check_procs -w 150 -c 200' : 'command[check_total_procs]=/usr/lib64/nagios/plugins/check_procs -w 800 -c 1200',
    'command[check_hda1]=/usr/lib64/nagios/plugins/check_disk -w 20% -c 10% -p /dev/hda1' : 'command[check_disk]=/usr/lib64/nagios/plugins/check_disk -w 8% -c 3% -p /',
  }

  for r in my_dict.keys():
    orig_nrpe_cfg = orig_nrpe_cfg.replace(r, my_dict[r])
  my_nrpe_cfg.write(orig_nrpe_cfg)
  
  # Set up any additional partitions given for disk checks
  partitions.remove("/")
  if len(partitions) > 0:
    my_nrpe_cfg.write("\n# Additional Disk Partition Checks Go Here\n")
    for p in partitions:
      if not os.path.exists(p):
        print "WARNING: %s does not exist. Skipping the check of this partition..." % p
      else:
        print "Adding Nagios NRPE disk usage check for %s partition..." % p
        p_name = re.sub('/', '', p, )
        print "NOTE: please make sure the \"check_%s command\" is defined on the server for this to work." % p_name
        p_chk_cmd = "command[check_" + p_name + "]=/usr/lib64/nagios/plugins/check_disk -w 8% -c 3% -p " + p + "\n"
        my_nrpe_cfg.write(p_chk_cmd)

  # Any 3ware or LSI raid controllers? Look for the cli utils
  # &  so set up monitoring...first the 3ware..
  if os.path.exists("/usr/local/bin/tw_cli"):
    print "tw_cli command located, setting up 3ware raid controller & disk checks..."
    # first try to download the check_3ware check utility
    try:
      response = urllib2.urlopen("http://spacewalk/pub/scripts/check_3ware-raid.pl")
      content = response.read()
      twpl = open( "/usr/local/sbin/check_3ware-raid.pl", 'w' )
      twpl.write(content)
      twpl.close()
      os.chmod("/usr/local/sbin/check_3ware-raid.pl", 0755)
      threeware_chk_cmd = ("\n# 3ware RAID Controller Checks\n"
      "command[check_3ware]=sudo /usr/local/sbin/check_3ware-raid.pl -a unit_check -p\n"
      "command[check_3ware_disks]=sudo /usr/local/sbin/check_3ware-raid.pl -a disk_check -p\n")
      my_nrpe_cfg.write(threeware_chk_cmd)
    # nrpe check_3ware-raid.pl needs to be added to sudoers...but only if it is not there!
      sudoersfile =  open("/etc/sudoers", 'ra+')
      nrpe_sudo_3w = False
      for line in sudoersfile:
        if re.search(r'^nrpe.*check_3ware-raid.pl$', line):
          nrpe_sudo_3w = True
      if not nrpe_sudo_3w:
        nrpe_sudoers_3w = ("\n## This will allow Nagios monitoring of 3ware RAID controllers\n"
        "nrpe ALL=(ALL) NOPASSWD:/usr/local/sbin/check_3ware-raid.pl\n")
        sudoersfile.write(nrpe_sudoers_3w)
        sudoersfile.close()
      else:
        print "nrpe user is already in sudoers for executing the 3ware check script (skipping the edit of this file).."
    except urllib2.URLError:
      print "ERROR: could not download check_3ware-raid.pl utility"
  # Same thing, but now for LSI MegaRAID
  # yes, this code dupe is lame, will re-work it at some point..
  if os.path.exists("/opt/MegaRAID/storcli/storcli64"):
    print "MegaCli64 command located, setting up MegaRAID health check..."
    # first try to download the MegaRAID check utility
    try:
      response = urllib2.urlopen("http://spacewalk/pub/scripts/check_lsismart")
      content = response.read()
      mgpl = open( "/usr/local/sbin/check_lsismart", 'w' )
      mgpl.write(content)
      mgpl.close()
      os.chmod("/usr/local/sbin/check_lsismart", 0755)
      megaraid_chk_cmd = ("\n# LSI MegaRAID Controller Checks\n"
      "command[check_lsismart]=/usr/local/sbin/check_lsismart\n")
      my_nrpe_cfg.write(megaraid_chk_cmd)
    # nrpe check_lsismart needs to be added to sudoers if not present
      sudoersfile =  open("/etc/sudoers", 'ra+')
      nrpe_sudo_mega = False
      for line in sudoersfile:
        if re.search(r'^nrpe.*check_lsismart$', line):
          nrpe_sudo_mega = True
      if not nrpe_sudo_mega:
        nrpe_sudoers_mega = ("\n## This will allow Nagios monitoring of LSI MegaRAID controllers\n"
        "nrpe ALL = NOPASSWD: /opt/MegaRAID/storcli/storcli64\n")
        sudoersfile.write(nrpe_sudoers_mega)
        sudoersfile.close()
      else:
        print "nrpe user is already in sudoers for executing the lsi megaraid check script (skipping the edit of this file).."
    except urllib2.URLError:
      print "ERROR: could not download check_lsismart utility"

  # This setting needs to be disabled else the 3ware or MegaRAID checks won't work from sudoers
  for line in fileinput.FileInput("/etc/sudoers",inplace=1):
    line = line.replace("Defaults    requiretty","#Defaults    requiretty")
    print line.rstrip()

  # We're done - close & rename cfg files, turn on nrpe & start daemon
  my_nrpe_cfg.close
  os.rename("/etc/nagios/nrpe.cfg", "/etc/nagios/nrpe.cfg.install")
  os.rename("/etc/nagios/nrpe.cfg.new", "/etc/nagios/nrpe.cfg")
  os.system("chkconfig nrpe on")
  os.system("service nrpe start")


def main(argv=sys.argv): 

  # Are we root?
  if os.geteuid() != 0:
    exit("You need to either use sudo or be root to execute this script.")
    
  try:
    opts, args = getopt.getopt(argv, "hp:s:", ["help", "partitions", "server"])
  except getopt.GetoptError:
    usage()
    sys.exit(2)

  # default values
  partitions = ["/"]
  nagios_svr = "10.0.0.10"

  
  # determine arguments from command line
  for opt, arg in opts:
    if opt in ("-h", "--help"):
      usage()
      sys.exit()
    elif opt in ("-p", "--partitions"):
      partitions.extend(arg.split())
    elif opt in ("-s", "--server"):
      nagios_svr = arg

  install_nrpe()
  configure_nrpe(nagios_svr, partitions)
    

# Execute Main Here
if __name__ == "__main__":
  main(sys.argv[1:])
