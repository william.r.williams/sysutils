#!/usr/bin/python

import sys, os, argparse, subprocess, time

""" figure out from the command line what file system we are to check
"""
parser = argparse.ArgumentParser()
parser.add_argument('-f', action="store", dest="filesystem")
script_args = parser.parse_args()
filesys = script_args.filesystem

def usage():
  print "\nUsage: " + sys.argv[0] + """ [ -f ] mounted_filesystem\n
  This script is meant to be run as a plugin for the Linux Nagios NRPE agent.
  It requires the name of a mounted file system as a single argument for "-f"
  """

def chk_mount(fs):

  """ Is the filesystem found under /proc?
  """
  try:
    mounted_fs = subprocess.check_output(["grep", fs, "/proc/mounts"])
  except:
    print "error: \'%s\' not found in /proc/mounts" % fs
    sys.exit(1)

  """ If we are good to this point open a file,
      try to write to it, close it, reopen and read
  """
  if mounted_fs:
    testfile = open(fs + "/" + "testfile.txt", "w")
    testfile.write("this file last written to at %s\n" % time.strftime("%d%b%Y%H%M%S%Z"))
    testfile.close
    print "ok"
  else:
    print "error: could not open test file under %s" % fs
    sys.exit(1)


if __name__ == "__main__":

  if os.geteuid() != 0:
    exit("You need to either use sudo or be root to run this script.")

  if not filesys:
    usage()
    sys.exit(3)

  chk_mount(filesys)
