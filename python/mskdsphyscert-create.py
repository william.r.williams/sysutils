#!/usr/bin/python

# mskdmfgca-ep-mskdphyscert-create.py
# This script:
#    -takes EP mac addresses as arguments either on the command line or from text file
#    -signs the original csr files (pushed nightly over from mfgca)
#    -creates new MDS-supported keys, csrs, & pem files, packaging each (e.g. 52540045C8C7.tgz)
#    -includes the other MDS-migration support files (trust.pem, ep-mskd-upgrade-wrapper.sh, & mdn_prod_staging.py) in said .tgz package
#    -copies each respective .tgz file to its respective production EP if it is on the msknet
#    -successes & failures are tracked by the log file (mskdphyscert-create-mskdmfgca-ep.txt)
#
# Non-zero exit codes:
# 1 - file mapping endpoints' MAC addresses to IP addresses not found
# 2 - script called with invalid argument
# 3 - can't reach MSKNET
#
#
#    -Bill W. Sept 2013

import os
import sys
import getopt
import re
import logging
import time
from sys import stderr


def usage():
    print sys.argv[0] +""": This script converts the MSK endpoint's original Certificate Signing Request (CSR)
                  into a physical certificate for MSK Directory Services (MDS) \n
          Accepts any of the following arguments:
          -h or --help                 (Print this Help Guide)
          -l or --log=                 (Specify the logfile to use - default is './mskdphyscerts-create.log')
          -m or --macs=                (Quoted list of MAC addresses, separated by spaces)
          -f or --file=                (Specify a file to read in MAC addresses for MDS cert upgrades)
          -a or --agency=              (Name of agency to be used for generating a list of endpoint MAC addresses for MDS cert upgrades)
          -e or --eps=                 (File which maps endpoints' MAC addresses to IP addresses - Default is './EndpointsReport.csv')
          -s or --skip                 (Skip bringing MSKNET interface up) 
          -u or --unzip                (After copying untar and unzip the MDS cert .tgz package on each target endpoint)
          -r or --runon                (Create a time-stamped host file for later use. If '-a' is used the agency name will be included)
          """

def chk_4_epmap():
    if not os.path.exists(epmap):
        ep_err_msg = ("File mapping Mac addresses to IP Addresses ('%s') Was Not Found." % (epmap))
        stderr.write("ERROR: " + ep_err_msg + "\n")
        logging.error(ep_err_msg)
        sys.exit(1)


def msknet_interface(action, msknet_gw = "10.1.1.1"):
    nic = "eth0"
    if action == "up":
        os.system("ifup %s" % (nic))
        msknet_ping = os.system("ping -c 1 %s >/dev/null" % (msknet_gw))
        if msknet_ping == 0:
            return True
    elif action == "down":
        os.system("ifdown %s" % (nic))
        return True
    else:
        return False


def main(argv):
    if len(sys.argv) == 1:
        usage()
        sys.exit()

    # Defaults for MDS
    logfile = "mskdphyscerts-create.log"
    global epmap
    epmap = "EndpointsReport.csv"
    global csr_dir
    csr_dir = "/opt/csrs/"
    outbound_tgz_dir = "/opt/mskd_tgzs/outbound/"
    deployed_tgz_dir = "/opt/mskd_tgzs/deployed/"
    global macaddrs
    macaddrs =""
    global macfile
    macfile = ""
    global macaddrs_from_file
    macaddrs_from_file = []
    global macaddrs_cmd_line
    macaddrs_cmd_line = []
    global agencyname
    agencyname = ""
    global agencymaclist
    agencymaclist = []
    global fullmaclist_clean
    fullmaclist_clean = []
    global macs_ips
    macs_ips = {}
    mskdjar="/opt/msk/mskd/latest/bin/mskd.jar"
    global skip_msknet
    skip_msknet = ""
    global un_zip
    un_zip = ""
    create_hostfile = ""

    try:
        opts, args = getopt.getopt(argv, "hl:m:f:e:a:sur", ["help", "log=", "macs=", "file=", "--eps", "--agency", "--skip", "--unzip", "--runon"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-l", "--log"):
            logfile = arg
        elif opt in ("-m", "--macs"):
            macaddrs = arg
        elif opt in ("-f", "--file"):
            macfile = arg
        elif opt in ("-e", "--eps"):
            epmap = arg
        elif opt in ("-a", "--agency"):
            agencyname = arg
        elif opt in ("-s", "--skip"):
            skip_msknet = "yes"
        elif opt in ("-u", "--unzip"):
            un_zip = "yes"
        elif opt in ("-r", "--runon"):
            create_hostfile = "yes"


    # OK, let's begin. Open the log file...
    logging.basicConfig(filename=logfile, level=logging.INFO)
    logging.info(("%s executed at %s") % (sys.argv[0], time.strftime("%a %d%b%Y %H:%M:%S %Z")))

    # verify the epmap is present
    chk_4_epmap()

    # bring msknet interface up - if this fails exit out as we won't be able to do what matters
    if not skip_msknet:
        msknet_ready = msknet_interface("up")
        if not msknet_ready:
            no_msknet_msg = ("Unable to ping default gateway to MSKNET")
            stderr.write("ERROR: " + no_msknet_msg + "\n")
            logging.error(no_msknet_msg)
            sys.exit(3)


    # make a list of all mac addrs given from the command line and/or file
    # first check if specified on the command line
    if macaddrs:
        macaddrs_cmd_line_tmp = re.split('\s', macaddrs)
        macaddrs_cmd_line = filter(None, macaddrs_cmd_line_tmp)
    # now see if a mac addr file was specified
    if os.path.exists(macfile):
        open_macfile = open(macfile, "r")
        macfilelist = open_macfile.readlines()
        for m in macfilelist:
            macaddrs_from_file.append(m.rstrip())
    if agencyname:
        agencymaclist = os.popen("grep ^%s %s | awk 'BEGIN {FS=\",\"} {print $2}'" % (agencyname, epmap)).read().rstrip().split()
    # combine both mac addr lists if both methods were used
    fullmaclist = macaddrs_cmd_line + macaddrs_from_file + agencymaclist
    # if it's an empty list, exit
    # check each item in the list for proper mac addr format (e.g. 12 hex digits)
    # first check if specified on the command line
    if fullmaclist:
        for mac in fullmaclist:
            mac_hex = mac.upper()
            if re.match(r'[A-F0-9][A-F0-9][A-F0-9][A-F0-9][A-F0-9][A-F0-9][A-F0-9][A-F0-9][A-F0-9][A-F0-9][A-F0-9][A-F0-9]', mac):
                fullmaclist_clean.append(mac_hex)
            else:
                bad_mac_msg = ("'%s' is not a valid MAC address - ignoring it" % (mac))
                print "WARNING: " + bad_mac_msg
                logging.warning(bad_mac_msg)
            
        logging.info(("started with this list of mac addresses: %s") % fullmaclist_clean)

    # locate corresponding csr file to each mac addr, upgrade original csr to mskd, make tgz file for EP in the outbound_tgz_dir
    for c in fullmaclist_clean:
       # check for duplicate EP serial numbers a.k.a MAC addresses
        numbmacs = os.popen("grep %s %s | wc -l" % (c, epmap)).read().rstrip()
        if int(numbmacs) > 1:
            dupmacs = os.popen("grep %s %s" % (c, epmap)).read().rstrip()
            dup_mac_msg = ("Multiple entries for an endpoint serial number (MAC address) found: %s" % (c))
            print "ERROR: " + dup_mac_msg
            logging.error(dup_mac_msg)
            print dupmacs
            fix_dup_mac_msg = ("Please resolve in %s before continuing." % (epmap))
            print fix_dup_mac_msg
            logging.error(dupmacs)
            logging.error(fix_dup_mac_msg)
            sys.exit(4)
        # skip upgrading certs if there's already a .tgz pkg for a given MAC addr in the deployed directory
        if os.path.exists(deployed_tgz_dir + c + ".tgz"):
            found_deployed_tgz_msg = (deployed_tgz_dir + c +".tgz already exists (Please verify whether this upgraded cert has already been deployed)")
            print "INFO: " + found_deployed_tgz_msg
            logging.info(found_deployed_tgz_msg)
            remove_deployed = fullmaclist_clean.index(c)
            del fullmaclist_clean[remove_deployed]
        # also skip upgrading certs if there's an existing .tgz pkg for a given MAC addr in the outbound directory
        # To Do: a retry option to copy...
        elif os.path.exists(outbound_tgz_dir + c + ".tgz"):
            found_outbound_tgz_msg = (outbound_tgz_dir + c + ".tgz already exists. Please use the retry option to copy to target")
            print "INFO: " + found_outbound_tgz_msg
            logging.info(found_outbound_tgz_msg)
            remove_existing = fullmaclist_clean.index(c)
            del fullmaclist_clean[remove_existing]
        fullpath_old_csr = csr_dir + c + ".csr"
        if os.path.exists(fullpath_old_csr) and not os.path.exists(outbound_tgz_dir + c):
            os.mkdir(outbound_tgz_dir + c)
            newcertdir = outbound_tgz_dir + c
            os.system("java -cp %s com.msk.mskd.pki.CertUtil upgrade-physical admin admin %s %s" % (mskdjar, fullpath_old_csr, newcertdir))
            os.system("pushd %s >/dev/null; tar czhf %s.tgz ep-mskd-upgrade-wrapper.sh trust.pem mdn_prod_staging.py ep-logical-syncpartner.sh %s; popd >/dev/null" % (outbound_tgz_dir, c, c))
            if os.path.exists(outbound_tgz_dir + c + ".tgz"):
                tgz_created_msg = ("successfully created %s.tgz under %s" % (c, outbound_tgz_dir))
                print "INFO: " + tgz_created_msg
                logging.info(tgz_created_msg)
                # map mac addrs to their respective EPs so the corresponding tgzs can be uploaded - dictionary a.k.a hash
                mac_ip = os.popen("grep %s %s | awk -F, '{print $2, $5}'" % (c, epmap)).read().rstrip().split()
                # test if anything was found and  see if it matches an ip addr, if so add it to the dictionary
                if mac_ip and re.match(r'\d*\.\d*\.\d*\.\d*', mac_ip[1]):
                    macs_ips[mac_ip[0]] = mac_ip[1] 
            else:
                no_tgz_found_msg = ("%s.tgz was not found under %s - Was it not successfully created?" % (c, outbound_tgz_dir))
                print "WARNING: " + no_tgz_found_msg
                logging.warning(no_tgz_found_msg)
        elif not os.path.exists(fullpath_old_csr):
            csr_notfound_msg = ("'%s.csr' was not found under %s on this server - it will need to be located & put in this directory before being upgraded" % (c, csr_dir))
            print "Error: " + csr_notfound_msg
            logging.error(csr_notfound_msg)
        elif os.path.exists(outbound_tgz_dir + c):
            outbound_csr_dir_exists = ("'The %s%s' directory already exists! Has this csr been previously signed? Please investigate & check file system for %s.tgz" % (outbound_tgz_dir, c, c))
            print "WARNING: " + outbound_csr_dir_exists
            logging.warning(outbound_csr_dir_exists)

    # Create a file of hosts for use with subsequent upgrade steps to follow after this script has executed
    if create_hostfile:
        if agencyname:
            hostfilename = agencyname + "_" + time.strftime("%d%b%Y_%H:%M:%S_%Z")
        else:
            hostfilename = "hosts_" + time.strftime("%d%b%Y_%H:%M:%S_%Z")
        hostfile = open(hostfilename, "w")

    # copies tgzs to their respective EPs & if successful move tgz pkgs from outbound_tgz_dir to deployed_tgz_dir
    # the 'try' command is so we can get out of this program in the event something's not going well
    try:
        for k, v in macs_ips.items():
            if create_hostfile:
                hostfile.write("%s\n" % (v))
            mskd_tgz = outbound_tgz_dir + k + ".tgz"
            ping_ep = os.system("ping -c 1 %s >/dev/null" % (v))
            # verify mskd tgz pkg is there
            if not os.path.exists(mskd_tgz):
                mskd_tgz_notfound_msg = ("'%s.tgz' was not found under %s on this server - it will need to be located before being copied to its respective endpoint" % (k, outbound_tgz_dir))
                print "Warning: " + mskd_tgz_notfound_msg
                logging.warning(mskd_tgz_notfound_msg)
            # check whether we can successfully ping the target host
            elif ping_ep != 0:
                no_ping_ep_msg = ("Endpoint %s seems to be unreachable - please verify it is online before copying its mskd certificate pkg (%s) to it" % (v, mskd_tgz))
                print "Warning: " + no_ping_ep_msg
                logging.warning(no_ping_ep_msg)
            # well then let's go for it - use 'copytoeps' so we are not prompted for a password!
            elif os.path.exists(mskd_tgz) and ping_ep == 0:
                os.system("copytoeps %s %s /home/support" % (v, mskd_tgz))
                copied_mskd_tgz_msg = ("copied %s to %s" % (mskd_tgz, v))
                print "INFO: " + copied_mskd_tgz_msg
                logging.info(copied_mskd_tgz_msg)
                # if that worked move the mskd_tgz to the deployed directory to help us keep track
                os.system("mv %s %s" % (mskd_tgz, deployed_tgz_dir))
                mv_mskd_tgz_msg = ("%s moved to %s" % (mskd_tgz, deployed_tgz_dir))
                print "INFO: " + mv_mskd_tgz_msg
                logging.info(mv_mskd_tgz_msg)
                if un_zip:
                    os.system("runoneps %s \"sudo tar zxf %s.tgz \"" % (v, k))
    except KeyboardInterrupt:
        ctl_c_msg = ("Exiting script per user 'ctl + c' command")
        print "INFO: " + ctl_c_msg
        logging.info(ctl_c_msg)
        sys.exit(5)

    # Be polite & close this file if it was opened
    if create_hostfile:
        hostfile.close()

    # ok all done bring down msknet interface
    msknet_interface("down")

    # Ok, We're Done. Log it as Finished.
    logging.info('Finished')

    # To Do List:
    # do not try and bring up eth0 if it is already up
    # fix bug where more than one mac addr may be detected in Endpoints csv file - at least filter this in advance for now?
    # a re-try option for copying created tgzs which didn't get uploaded the first time around? (i.e. in case the EP was down)
    # Use a 'prefix' option for beginning of mac addr?

# Main
if __name__ == "__main__":
    main(sys.argv[1:])
