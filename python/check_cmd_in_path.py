#!/usr/bin/python

import os
import sys
import getopt
import re

# python function to make certain system cmds are present

def chk_sys_cmd(*argv):

  # break this apart and search each location
  places = os.environ['PATH'].split(':')

  for arg in argv:
    utilfound = ""
    isexecutable = False
    for p in places:
      if os.path.exists(p + "/" + arg):
        utilfound = p + "/" + arg
        if os.access(utilfound, os.X_OK):
          isexecutable = True
          print "OK %s utility found appears to be executable..." % utilfound
    if not utilfound:
      print "ERROR: %s utility was not found" % arg
      sys.exit(5)
    elif not isexecutable:
      print "ERROR: %s utility found but not executable?" % utilfound
      sys.exit(5)


chk_sys_cmd("md5sum", "foobar")
