#!/usr/bin/python
# mdn_prod_staging.py - a script created with the intent
# to help simplify the configuring a MSK Directory Node (MDN)
# within the production environment.
# Generally this is done in two parts: first the mdn logical cert request
# then configuring a sync partner & subsequent EP logical cert req
#
# Non-zero exit codes:
# 1 - non-mds version of the MSK Application
# 2 - script called with no arguments
# 3 - java not found
# 4 - mds jar file not found
# 5 - mskcertreq script not found
# 6 - mdn physical cert request in a pending state
# 7 - could not load mds keystore
#
# -Bill W. july 2013


import os
import sys
import getopt
import re
from sys import stderr

# Set the path up for our environment
MLJAVA_BIN = '/usr/java/default/bin'
MLUTILS = '/opt/msk/latest/sbin/'
os.environ['PATH'] += os.pathsep + MLJAVA_BIN + os.pathsep + MLUTILS

def usage():
    print sys.argv[0] +""": This script configures a MSK Directory Node (MDN)
                  to work within the MSK Directory Services (MDS) production environment.\n
          Accepts any of the following arguments (of which at least one is needed):
          -h or --help                 (Print this Help Guide)
          -d or --setdefaults          (Set default values for masterinterval sync, crl refresh rate, and CA port)
          -i or --importtrust          (Import trust file - default is '/opt/msk/mds/latest/bin/trust.pem')
          -p or --importtrustfile=     (Override default trust file location)
          -s or --sync-masterinterval= (Setting for sync.masterinterval - default is 1min)
          -r or --crl-refresh=         (Refresh rate for the Certificate Revocation List - default is 1min)
          -c or --ca-address=          (Address of the Certificate Authority Server)
          -a or --ca-port=             (TCP port of the Certificate Authority Server - default is 9302)
          -l or --crlip-address=       (Address of the Certificate Revocation List Server)
          -t or --tip-address=         (Address of the Trusted IP Server) 
          -k or --keystore-password=   (Change the Java keystore password)
          -R or --requestmdnlogical    (Configure and send an MDN logical cert request)
          -S or --syncpartner          (Configure a Sync Partner)
          -N or --syncp-name=          (Specify the name of the Sync Partner)
          -D or --syncp-domain=        (Specify the name of the Sync Domain)
          -U or --syncp-url=           (Specify the IP address of the Sync Partner)
          -P or --syncp-port=          (Specify the port of the Sync Partner - default is 9301)
          -G or --syncp-group=         (Specify the Sync Group - default is 'Other')
          -M or --syncp-ifmaster=      (Specify if Master - default is False)
          -B or --syncp-backup=        (Specify Backup Sync Parter - default is None)
          -I or --syncp-interval=      (Specify interval for Syncronization - default is 15min)
          -L or --requesteplogical     (Configure and Send a logical cert request for an End Point)
          -m or --setmcc               (Set MDS-Compatible MCC Parameters in msk.cfg)
          -A or --all-in-one=          (Sets ca-address, crlip-address, and tip-address all to the same ip)
          -o or --domain               (Specify a Domain to use - default is 'mnet')
          -T or --syncp-type=          (Set Sync Partner MDN type - default is 2 a.k.a 'Domain_MDN') 
          """

def chk_app_ver():
    mdsversion = ""
    mskverfile="/opt/msk/latest/MSKVersion"
    if os.path.exists(mskverfile):
        version = open(mskverfile, 'r')
        version_line1 = version.readline()
        verstring = version_line1.rstrip().split('.')
        if int(verstring[0]) >= 2:
            mdsversion = "true"
    if mdsversion:
        print "Beginning configuration of MDN for MSK Directory Services..."
    else:
        stderr.write("ERROR: Current MSK version (%s) does not support MDS\n"
        % (version_line1.rstrip()))
        sys.exit(1)

def confirm(resp=True):

    prompt = '[%s]|%s: ' % ('y', 'n')

    while True:
        ans = raw_input(prompt)
        if not ans:
            return resp
        if ans not in ['y', 'Y', 'n', 'N']:
            print 'please enter y or n.'
            continue
        if ans == 'y' or ans == 'Y':
            return True
        if ans == 'n' or ans == 'N':
            return False

def chk_mskcertreq():
    global mskcertreq
    mskcertreq = "/opt/msk/mds/latest/bin/mskcertreq"
    if not os.path.exists(mskcertreq):
        stderr.write("ERROR: The mskcertreq script was not found.\n")
        sys.exit(5)
    mdn_pending_cert = os.popen("/opt/msk/mds/latest/bin/mskpki-show mdn | grep -i pending").read().rstrip()
    if mdn_pending_cert:
        stderr.write("""
                 ERROR: An MDN logical certificate request is in the 'pending' state.
                 Another request cannot be made until the pending request is approved
                 at the Domain Controller.\n\n""")
        sys.exit(6)

def chk_keystore():
    global keystore_unable_load
    keystore_unable_load = os.popen("/opt/msk/mds/latest/bin/mskpki-show mdn 2>&1 | grep 'unable to load certificate'").read().rstrip()
    if keystore_unable_load:
        stderr.write("""ERROR:  unable to load certificate(s) for this MDN's keystore;
                        perhaps the trust file has not been imported?\n\n""")
        sys.exit(7)

def get_name_agency():
    epuri = os.popen("mskcfg get OurIdentity").read().rstrip()
    epuri_fields = epuri.split("/")
    global epname
    epname = epuri_fields[2]
    global epagency
    epagency = epuri_fields[1]


def main(argv):

    if len(sys.argv) == 1:
        usage()
        sys.exit()

    # Defaults for MDS
    importtrust = ""
    importtrustfile = "/opt/msk/mds/latest/bin/trust.pem"                
    sync_masterinterval = ""
    crl_refresh = ""
    ca_address = ""
    ca_port = ""
    crlip_address = ""
    tip_address = ""
    keystore_password = ""
    requestmdnlogical = ""
    setdefaults = ""
    syncpartner = ""
    syncpname = ""
    syncpdomain = ""
    syncpurl = ""
    syncpport = 9301
    syncpgroup = "Other"
    syncpifmaster = "false"
    syncpbackup = "\"\""
    syncpinterval = 15
    syncpmdntype = 2
    requesteplogical = ""
    global domain
    domain = "mnet"
    set_mcc = ""
    allinone = ""
    full_mdn_string = ""
    global epname
    epname = ""
    global epagency
    epagency = ""


    try:
        opts, args = getopt.getopt(argv, "hdip:s:r:c:a:l:t:k:RSN:D:U:P:G:M:B:I:LmA:o:T:", ["help", "setdefaults", "importtrust", "importtrustfile=", "sync-masterinterval=",
        "crl-refresh=", "ca-address=", "ca-port=", "crlip-address=", "tip-address=", "keystore-password=", "requestmdnlogical",
        "syncpartner", "syncp-name=", "syncp-domain=", "syncp-url=", "syncp-port=", "syncp-group=", "syncp-ifmaster=", "syncp-backup=", "syncp-interval=",
        "requesteplogical", "setmcc", "all-in-one=", "domain=", "syncp-type="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-d", "--setdefaults"):
            setdefaults = "yes"
        elif opt in ("-i", "--importtrust"):
            importtrust = "yes"
        elif opt in ("-p", "--importtrustfile"):
            importtrustfile = arg
        elif opt in ("-s", "--sync-masterinterval"):
            sync_masterinterval = arg
        elif opt in ("-r", "--crl-refresh"):
             crl_refresh = int(arg) * 60000
        elif opt in ("-c", "--ca-address"):
            ca_address = arg
        elif opt in ("-a", "--ca-port"):
            ca_port = arg
        elif opt in ("-l", "--crlip-address"):
            crlip_address = arg
        elif opt in ("-t", "--tip-address"):
            tip_address = arg
        elif opt in ("-k", "--keystore-password"):
            keystore_password = arg
        elif opt in ("-R" or "--requestmdnlogical"): 
            requestmdnlogical = "yes"
        elif opt in ("-S" or "--syncpartner"): 
            syncpartner = "yes"
        elif opt in ("-N", "--syncp-name"):
            syncpname = arg
        elif opt in ("-D", "--syncp-domain"):
            syncpdomain = arg
        elif opt in ("-U", "--syncp-url"): 
            syncpurl = arg
        elif opt in ("-P", "--syncp-port"):
            syncpport = arg
        elif opt in ("-G", "--syncp-group"):
            syncpgroup = arg
        elif opt in ("-M", "--syncp-ifmaster"): 
            syncpifmaster = arg
        elif opt in ("-B", "--syncp-backup"): 
            syncpbackup = arg
        elif opt in ("-I", "--syncp-interval"):
            syncpinterval = arg
            if not re.match(r'[0-9]*$', syncpinterval):
                syncpinterval = "15"
        elif opt in ("-L", "--requesteplogical"):
             requesteplogical = "yes"
        elif opt in ("-m", "--setmcc"):
             set_mcc = "yes"
        elif opt in ("-A", "--all-in-one"):
            allinone = arg
        elif opt in ("-o", "--domain"):
            domain = arg
        elif opt in ("-T", "--syncp-type"):
            syncpmdntype = arg
            if not re.match(r'[012]', syncpmdntype):
                syncpmdntype = "2"

    # First make certain MDS is supported in this version of the MSK Application
    chk_app_ver()

    # Test for java
    testjava = os.system("java -version 2> /dev/null")
    if not testjava == 0:
        stderr.write("ERROR: Java not found or invalid version\n")
        sys.exit(3)

    # Make sure the MDS jar file is available
    mdsjar = "/opt/msk/mds/latest/bin/mds.jar"
    if not os.path.exists(mdsjar):
        stderr.write("ERROR: file '%s' not found\n" % (mdsjar))
        sys.exit(4)

    # Set values for masterinterval sync, crl refresh rate; use defaults if that was specified

    if sync_masterinterval:
        print "Setting local MDN's masterinterval sync..."
        os.system("java -jar %s set sync.masterinterval %d" % (mdsjar, sync_masterinterval))
    elif setdefaults:
        sync_masterinterval = 1
        print "Setting local MDN's masterinterval sync to a default of 1 minute..."
        os.system("java -jar %s set sync.masterinterval %d" % (mdsjar, sync_masterinterval))

    if crl_refresh:
        print "Setting local MDN's crl refresh rate..."
        os.system("java -jar %s set crl.refresh %s" % (mdsjar, crl_refresh))
    elif setdefaults:
        crl_refresh = 60000
        print "Setting local MDN's crl refresh rate to a default of 1 minute..."
        os.system("java -jar %s set crl.refresh %s" % (mdsjar, crl_refresh))

    # import the certificate trust chain
    if importtrust and os.path.exists(importtrustfile):
        print "Importing trust file..."
        os.system("java -cp %s com.msk.mds.pki.CertUtil importtrust admin admin %s" % (mdsjar, importtrustfile))
    elif importtrust and not os.path.exists(importtrustfile):
        print "WARNING: trust file '%s' not found." % (importtrustfile)

    # Set CA Address if it was defined
    if ca_address:
        print "Setting address of CA server to %s" % ca_address
        os.system("java -jar %s set ca.address %s" % (mdsjar, ca_address))
    elif allinone:
        print "Setting address of CA Server to %s" % allinone
        os.system("java -jar %s set ca.address %s" % (mdsjar, allinone))

    # Set CA port if it was defined
    if ca_port:
        print "Setting TCP port of CA server to %s" % ca_port
        os.system("java -jar %s set ca.port %s" % (mdsjar, ca_port))
    elif setdefaults:
        ca_port = 9302
        print "Setting TCP port of CA server to the default value of %s" % ca_port
        os.system("java -jar %s set ca.port %s" % (mdsjar, ca_port))

    # Set CRL Address if it was defined
    if crlip_address:
        print "Setting address of CRL server to %s" % crlip_address
        os.system("java -jar %s set crlip.address %s" % (mdsjar, crlip_address))
    elif allinone:
        print "Setting address of CRL server to %s" % allinone
        os.system("java -jar %s set crlip.address %s" % (mdsjar, allinone))

    # Set TIP Address if it was defined
    if tip_address:
        print "Setting address of trusted IP server to %s" % tip_address
        os.system("java -jar %s set tip.address %s" % (mdsjar, tip_address))
    elif allinone:
        print "Setting address of trusted IP server to %s" % allinone
        os.system("java -jar %s set tip.address %s" % (mdsjar, allinone))

    # Set Java Keystore Password if it was defined
    if keystore_password:
        print "Setting Java keystore password as specified on command line..."
        os.system("java -jar %s set keystore.password %s" % (mdsjar, keystore_password))

    # MDN Logical Cert Request Here
    if requestmdnlogical:
        chk_keystore()
        get_name_agency()
        mdn_name =  epname + "-mdn"
        mdn_cert_req = "%s %s %s %s %s" % (mdn_name, mdn_name.upper(), domain, domain, epagency)
        mdn_display_cert_req = domain + "." + epagency + "." + mdn_name
        print "MDN logical certificate requested. Is the string below acceptable? (Answering 'n' will allow you to edit)"
        print "%s " % mdn_display_cert_req,
        if confirm():
            pass
        else:
            user_ok = ""
            while user_ok != 'y':
                mdn_cert_req = raw_input("Enter the MDN's logical cert request string below (must be in 'domain.agency.name' format):\n")
                # Check user input for correct format
                if mdn_cert_req.count('.') < 2:
                    mdn_cert_req = ""
                    print "Please try again. Cert request not in 'domain.agency.name' format."
                else:
                    mdn_cert_req_fields = mdn_cert_req.split('.')
                    domain = mdn_cert_req_fields[0]
                    epname = mdn_cert_req_fields[-1]
                    del mdn_cert_req_fields[0]
                    del mdn_cert_req_fields[-1]
                    epagency = "".join(mdn_cert_req_fields)
                    mdn_name =  epname
                    mdn_cert_req = "%s %s %s %s %s" % (mdn_name, mdn_name.upper(), domain, domain, epagency)
                    mdn_display_cert_req = domain + "." + epagency + "." + mdn_name
                if mdn_cert_req:
                    print """Use '%s' for this MDN's logical cert request string?
                           Enter 'y' to confirm otherwise hit return and you will be able to edit)""" % mdn_display_cert_req
                    user_ok = raw_input().lower()

        os.system("java -cp %s com.msk.mds.pki.CertUtil request admin admin %s" % (mdsjar, mdn_cert_req))
        os.system("/sbin/service msk_mdsd restart")
        print "MDN Logical Certificate Request has been submitted."
        

    # Do we want to add a sync partner? If syncpname, syncpdomain, and syncpurl have been specified then a sync partner will be added
    # per the java command below...otherwise prompt for interactive confirmation for these & the other sync partner values
    if syncpname and syncpdomain and syncpurl:
        print "Configuring sync partner for this MDN..."
        os.system("java -jar %s syncpartner %s %s %s %s %s %s %s %s %s" % (mdsjar, syncpname, syncpdomain, syncpurl, syncpport, syncpgroup, syncpifmaster,
        syncpbackup, syncpinterval, syncpmdntype))
    elif syncpartner:
        syncpnameprompt = "Sync partner for this MDN: "
        if syncpname:
            print "Accept %s as the sync partner for this MDN?" % syncpname
            if confirm():
                pass
            else:
                syncpname = raw_input(syncpnameprompt)
        else:
            syncpname = raw_input(syncpnameprompt)

        syncpdomainprompt = "Sync domain for this MDN: "
        if syncpdomain:
            print "Accept %s as the sync domain name for this MDN?" % syncpdomain
            if confirm():
                pass
            else:
                syncpdomain = raw_input(syncpdomainprompt)
        else:
            syncpdomain = raw_input(syncpdomainprompt)

        syncpurlprompt = "URL of the sync partner for this MDN: "
        if syncpurl:
            print "Accept %s as the sync partner URL for this MDN?" % syncpurl
            if confirm():
                pass
            else:
                syncpurl= raw_input(syncpurlprompt)
        else:
            syncpurl = raw_input(syncpurlprompt)

        print "Sync partner port number for this MDN: %s"% syncpport
        if confirm():
            pass
        else:
            syncpport = raw_input("Please enter the port number of the Sync Partner to be used for this MDN: ")

        print "Sync group for this MDN: %s" % syncpgroup
        if confirm():
            pass
        else:
            syncpgroup = raw_input("Please enter the name of the Sync group to be used for this MDN: ")

        print "Sync if Master: %s" % syncpifmaster
        if confirm():
            pass
        else:
            syncpifmaster = raw_input("Specify 'true' if this MDN is to sync only as Master (NOTE: ONLY for already deployed systems): ")

        print "Sync Backup for this MDN (hit return for no backup): %s" % syncpbackup
        if confirm():
            pass
        else:
            syncpname = raw_input("Sync Backup for this MDN: ")

        print "Sync interval for this MDN: %s" % syncpinterval
        if confirm():
            pass
        else:
            syncpinterval = raw_input("Sync interval for this MDN: ")

        print "Sync Partner Type for this MDN: %s" % syncpmdntype
        if confirm():
            pass
        else:
            syncpmdntype = raw_input("Sync Partner Type for this MDN: ")

        print "Configuring Sync Partner..."
        os.system("java -jar %s syncpartner %s %s %s %s %s %s %s %s %s" % (mdsjar, syncpname, syncpdomain, syncpurl, syncpport, syncpgroup, syncpifmaster,
        syncpbackup, syncpinterval, syncpmdntype))


    # Last but not least - A logical cert request

    if requesteplogical:
        chk_keystore()
        chk_mskcertreq()
        get_name_agency()
        epuri = os.popen("mskcfg get OurIdentity").read().rstrip()
        fqdn = domain + "." + epagency + "." + epname
        epipaddr = os.popen("ifconfig eth0 | egrep 'inet addr|inet ' | sed -e 's/[^1-9]*//' | awk '{print $1}'").read().rstrip()
        print "Endpoint logical certificate requested. Is the string below acceptable? (Answering 'n' will allow you to edit)"
        print "%s " % fqdn,
        if confirm():
            pass
        else:
            user_ok = ""
            while user_ok != 'y':
                fqdn = raw_input("Enter the endpoint's logical cert request string below (Must be in 'domain.agency.name' format):\n")
                if fqdn.count('.') < 2:
                    fqdn = ""
                    print "Please try again. Cert request not in 'domain.agency.name' format."
                else:
                    fqdn_fields = fqdn.split('.')
                    domain = fqdn_fields[0]
                    epname = fqdn_fields[-1]
                    del fqdn_fields[0]
                    del fqdn_fields[-1]
                    epagency = "".join(fqdn_fields)
                    fqdn = domain + "." + epagency + "." + epname
                if fqdn:
                    print """Use '%s' for this endpoint's logical cert request string?
                           Enter 'y' to confirm otherwise hit return and you will be able to edit)""" % fqdn
                    user_ok = raw_input().lower()


        os.system("%s 1 %s %s %s %s" % (mskcertreq, fqdn, epname, epuri, epipaddr))
        print "Endpoint logical cert request has been submitted."

    if set_mcc:
        print "Setting MCC parameters to MDS-Supported Values..."
        os.system("mskcfg set MccAddr.Universal 224.9.0.1 set MccPresenceOnly.Universal true set MccKeepAlivePeriod.Universal 300")

 
# Main
if __name__ == "__main__":
    main(sys.argv[1:])
