#!/usr/bin/python

import os, sys
from datetime import timedelta

# this job is meant to be run every 5 minutes from cron. 
# if the system has been up for less than 5 minutes
# send a notification email

with open('/proc/uptime', 'r') as f:
    uptime_seconds = float(f.readline().split()[0])
    if uptime_seconds < 360:
        reboots = open("/root/reboots.txt", "r+")
        numreboots = int(reboots.readline())
        totalreboots = numreboots + 1
        reboots.seek(0)
        reboots.write(str(totalreboots) + "\n")
        reboots.close
        os.system("echo \"unplanned reboot number $(cat /root/reboots.txt)\" |\
        mail -s \"$(hostname): unplanned reboot detected\"william.r.williams@gmail.com")
