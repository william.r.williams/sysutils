#!/usr/bin/python
#
###################################################################################################
#   A generic script for checking a daemon through the Nagios NRPE agent
#   -Bill W. dec2015
#   william.r.williams@gmail.com                        
###################################################################################################


import sys, os, argparse, subprocess, re

""" figure out from the command line what daemon we are to check
"""
parser = argparse.ArgumentParser()
parser.add_argument('-d', action="store", dest="my_daemon")
script_args = parser.parse_args()

def chk_whether_root():
  if os.geteuid() != 0:
    exit("You need to either use sudo or be root to execute this script.")

def usage():
  print "\nUsage: " + sys.argv[0] + """ [ -d <daemon_name> ]\n
  This script is meant to be run as a plugin for the Nagios NRPE agent
  on Linux systems. It requires the following argument:
          -d <daemon_name>       Name of daemon to checked
  """
  sys.exit(2)


# First verify the daemon is present in the process list
def chk_daemon_is_running(daemon):
  if daemon:
    processes = subprocess.check_output(["ps", "aux"]).splitlines()
    daemon_found = None
    for proc in processes:
      if re.search(sys.argv[0], proc):
        pass
      elif re.search(daemon, proc):
        daemon_found = True
        proc_fields = proc.split()
        print ("%s running with PID %s" % (daemon, proc_fields[1]))
    if not daemon_found:
      print "%s is not running" % daemon
      sys.exit(1)



if __name__ == "__main__":

  chk_whether_root()

  if not (script_args.my_daemon):
    usage()
    sys.exit(1)

  chk_daemon_is_running(script_args.my_daemon)
