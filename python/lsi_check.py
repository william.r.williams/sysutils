#!/usr/bin/python
#
###################################################################################################
#   A script for checking an LSI RAID controller either on a Linux or Windows server.
#   Exit codes are meant for the Nagios NRPE agent. Anything returned besides '0' is an error state
#   -Bill W. jan2016
#   william.r.williams@gmail.com                         
###################################################################################################

import sys, os, re, getopt

# Are we root?
#if os.geteuid() != 0:
#  exit("You need to either use sudo or be root to execute this script.")

# find out where the megaRAID utility is - first off is this system linux or windows?
if sys.platform == 'linux2':
  megacli = "/opt/MegaRAID/storcli/storcli64"
elif sys.platform == 'win32':
  megacli = "\"c:\Program Files (x86)\MegaRAID Storage Manager\StorCli64\""
else:
  print "ERROR: Unknown Platform"
  sys.exit(4)

def usage():
  print "\nUsage: " + sys.argv[0] + """ [ -h --help -c --ctlr]\n
  This script is for checking an LSI RAID controller using the storcli64 command from either a Windows
  or Linux system. A Windows system will need python installed, virtually all Linux systems already have it.
          -h or --help            Print this help message
          -c or --ctlr=           The controller to check in multi-controller systems
                                  If omitted the first controller (/c0) is assumed.

  Exit Codes:
           0    All Good
           1    STDERR
           2    One or more physical drives are in a bad state
           3    MegaRAID storcli check failed (was a valid controller number given?)
           4    MegaRAID cli utility (storcli64) was not found
  """
  sys.exit(1)



def chk_lsi_phy_disks(controller):
  if megacli:
    try:
      status = os.popen(megacli + " /c" + str(controller) + " show").read().rstrip().splitlines()
      
      # these are known good drive states
      drv_online = re.compile('Onln.*HDD')
      drv_ded_hot_spare = re.compile('DHS.*HDD')
      drv_unconf_good = re.compile('UGood.*HDD')
      drv_glob_hot_spare = re.compile('GHS.*HDD')
      
      # how many drives do we have on this controller?
      phy_drives = re.compile('Physical Drives = ')
      good_drives_found = 0
      
      for line in status:
        if re.search(phy_drives, line):
          num_drives = re.sub(phy_drives,'',line)
          # confirm they are all in a good state
        if re.search(drv_online, line) or re.search(drv_ded_hot_spare, line) or re.search(drv_unconf_good, line) or re.search(drv_glob_hot_spare, line):
           good_drives_found += 1
      if int(num_drives) == good_drives_found:
        print "OK: All physical drives in a good state"
      else:
        print "ERROR: one or more physical drives found in a bad state"
        sys.exit(2)
    except:
      print "ERROR: MegaRAID storcli check failed"
      sys.exit(3)
  else:
    print "ERROR: MegaRAID cli utility 'storcli64' was not found"
    sys.exit(4)



def main(argv=None):

  controller = 0

  if argv is None:
    argv = sys.argv

  try:
     opts, args = getopt.getopt(argv, "hc:", ["help", "ctlr="])
  except getopt.GetoptError:
    usage()
    sys.exit(1)

  for opt, arg in opts:
    if opt in ("-h", "--help"):
      usage()
    elif opt in ("-c", "--ctlr"):
      controller = arg

  chk_lsi_phy_disks(controller)


if __name__ == "__main__":
  main(sys.argv[1:])
