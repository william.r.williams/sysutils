#!/usr/bin/python

# The purpose of this script is to filter and organize dmesg output when debugging
# arguments include string to look for (-s <string>), -i interval,
# and the time the script should run (-d duration)
# if duration argument is  used the script will run indefintely

import subprocess, time, os, re, argparse, sys

# need to be root or exit. Can't clear ring buffer w/o priviledged account.
if os.geteuid() != 0:
    exit("You need to be root to run this script.")

# parse any arguments here
parser = argparse.ArgumentParser()
parser.add_argument('-s', action="store", dest="search_string")
parser.add_argument('-i', action="store", dest="interval", type=int)
parser.add_argument('-d', action="store", dest="duration", type=int)
my_args = parser.parse_args()

if my_args.search_string:
    my_regex = my_args.search_string  + ".*"
else:
    my_regex = "[a-z].*"

if my_args.interval:
    interval = my_args.interval
else:
    interval = 10

if my_args.duration:
    if my_args.duration < my_args.interval:
        print "ERROR: check args - polling interval cannot be longer that the script duration"
        sys.exit(1)
    total_runtime = my_args.duration
else:
    total_runtime = 30

# back up the previous log file if it exists
if os.path.exists("dmesg.log"):
    os.rename("dmesg.log","dmesg.log.bak.%s" % time.strftime("%d%b%Y%H%M%S%Z"))

# open the log and put in a time stamp header
dmesgLog = open("dmesg.log", "w")
header = ("Begin logging output of dmesg at %s\n\n" % time.strftime("%a %d%b%Y %H:%M:%S %Z"))
print header.rstrip() + " to dmesg.log"
if my_args.duration:
    print "This script will run for %s seconds..." % my_args.duration
else:
    print "This script will run indefinitely until \'CTRL + C\' ends it..."
dmesgLog.write(header)


# generate, filter, and write the output to the log file

while total_runtime >= interval:
    dmesgLog = open("dmesg.log", "a+")
    dmesgLog.write("(%s)\n\n" % time.strftime("%H:%M:%S %Z %a %d%b%Y"))
    #dmesgout = subprocess.check_output(["dmesg", "-x", "-t", "-c"])
    dmesgout = subprocess.check_output(["dmesg", "-x", "-t"])
    my_match = re.findall(my_regex, dmesgout, re.MULTILINE)
    for line in my_match:
        dmesgLog.write("%s\n" % (line))
    dmesgLog.write("---" + "\n" * 2 )
    dmesgLog.close
    time.sleep(interval)
    if my_args.interval and my_args.duration:
        total_runtime -= interval
