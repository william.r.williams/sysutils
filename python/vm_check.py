#!/usr/bin/python

import subprocess, sys

VM = "win7wkstation"
state = subprocess.check_output(["virsh", "domstate", VM], stderr=None).rstrip()

if state == "running":
  print VM + " is running"
  sys.exit(0)
elif state == "shut off":
  print VM + " is shut off"
  sys.exit(2)
else:
  print "Oh No!"
  sys.exit(3)

