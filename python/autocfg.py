#!/usr/bin/python

# we need to find the epmfg.cfg file.
# most likely this is on a connected usb drive but not mounted yet...

# first let's determine what's available and put it into a list
# recall that all usb drives show up as sd* devices under linux


import sys, os, re
from os.path import exists, join
from os import pathsep
from string import split

def find_cfgfile():
    drivelist = os.popen("awk '{print $4}' /proc/partitions | grep sd | grep -v [1-9]").read()
    drives = drivelist[0:-1].split("\n")

    # if sda is already mounted, then it is probably the boot disk
    # so we must exclude it from our list

    sda_mounted = os.popen("mount -l | grep boot | grep sda").read()

    if sda_mounted:
        for drive in drives:
            if drive == "sda":
                remove_sda = drives.index(drive)
                if str(remove_sda):
                    del drives[remove_sda]

    # let's mount the remaining sd* partitions & look for the config file
    # exit when found and return the file with it's absolute path
    # if still not found then search the entire file system

    # if this doesn't exist make the temporary mount point
    if not os.path.exists("/mnt/epmfgdir"):
        os.system("mkdir /mnt/epmfgdir")

    for drive in drives:
        device = ("/dev/" + drive + "1")
        if os.path.exists(device):
            os.system("mount " + device + " /mnt/epmfgdir")
            epmfg_file =  os.popen("find /mnt/epmfgdir -name epmfg.cfg").read()
            if epmfg_file:
                return epmfg_file.rstrip()
                break
        if os.path.ismount("/mnt/epmfgdir"):
            os.system("umount -f /mnt/epmfgdir")

    # This is for legacy nic systems, where the usbdrive shows up as /dev/usbdrive...
    if os.path.exists("/dev/usbdrive"):
        os.system("mount /dev/usbdrive /mnt/epmfgdir")
        epmfg_file =  os.popen("find /mnt/epmfgdir -name epmfg.cfg").read()
        if epmfg_file:
            return epmfg_file.rstrip()

    # Last Chance - try to find the configuration file elsewhere on the file system
    epmfg_file =  os.popen("find / -name epmfg.cfg").read()
    if epmfg_file:
        return epmfg_file.rstrip()

# MAIN

systemusers = ("root", "support")
mskcfg_cmd = "/opt/msk/latest/sbin/mskcfg"
genmskparams = ("OurIdentity", "MulticastAddr", "CliPassword", "PttComPort")
kdsmskparams = ("KdsRemoteAddress")
nmsmskparams = ("NmsIpAddress")
emsadmin = "admin"
webaccessfile = "/opt/msk/latest/html/config/.access"
openssl_cmd = os.popen("which openssl").read().rstrip()
previously_created_epmfgdir = os.path.exists("/mnt/epmfgdir")
epcfgfile = find_cfgfile()
epcfgdict = {}

if epcfgfile:
    cfgfile =  open(epcfgfile, 'r')
    for line in cfgfile:
        if re.search(r"\s", line.rstrip()):
            (key, val) = line.split(None, 1)
            epcfgdict[str(key)] = val
else:
    print ("End Point Configuration File Not Found.")

for k, v in epcfgdict.items():

    # Is a password code defined?
    # if so create a hint file for a privileged user only
    if re.match( '%%', k ):
        pwcodefile = open("/root/mskpw.hint", "w")
        pwcodefile.write("%s" % (v))
        pwcodefile.close
        os.system("chmod 400 /root/mskpw.hint")
        del epcfgdict[k]

    # First change any system level accounts
    if k in systemusers:
        os.system("/bin/echo %s:%s | /usr/sbin/chpasswd" % (k, re.escape(v)))

   # Next change webtools passwd
    if k == emsadmin and os.path.isfile(webaccessfile) and os.path.isfile(openssl_cmd):
        new_emsadmin_passwd_hash = os.popen("echo -n " + (re.escape(v)) + "| " + openssl_cmd + " dgst -md5").read().rstrip()
        os.system("sed -e 's/^admin.*RW$/admin;" + new_emsadmin_passwd_hash + ";RW/' -i " + webaccessfile)

    # Set MSK Application Parameters here if found
    if k in genmskparams and os.path.exists(mskcfg_cmd):
          os.system(mskcfg_cmd + " set %s %s" % (k,re.escape(v)))
    elif k in kdsmskparams and os.path.exists(mskcfg_cmd):
          os.system(mskcfg_cmd + " sect KDS set %s %s" % (k, re.escape(v)))
    elif k in nmsmskparams and os.path.exists(mskcfg_cmd):
          os.system(mskcfg_cmd + " sect NMS set %s %s" % (k, re.escape(v)))

# Clean Up before Exiting
if previously_created_epmfgdir == False and os.path.ismount("/mnt/epmfgdir"):
    os.system("umount -l /mnt/epmfgdir")
    os.system("rm -rf /mnt/epmfgdir")
