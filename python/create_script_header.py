#!/usr/bin/python

# initialize a new python script with a consistent header

import os, sys, time, argparse

date_init = time.strftime("%d%b%Y")
deft_scriptname = os.getlogin() + "_script_" + date_init + "-" + time.strftime("%H%M%S%Z") 

parser = argparse.ArgumentParser(description="collect arguments for" + sys.argv[0])

# Set defaults here otherwise generate based on the username and timestamp
deft_interpreter = "#!/bin/bash"
deft_author = "Bill W."
deft_org = "GBC (Grafton Beer Cooperative)"
deft_comment = "Please describe what this script does."
deft_email = "william.r.williams@gmail.com"

parser.add_argument('-i','--interpreter', action="store", dest="interpreter", default=deft_interpreter)
parser.add_argument('-s','--scriptname', action="store", dest="scriptname", default=deft_scriptname)
parser.add_argument('-a','--author', action="store", dest="author", default=deft_author )
parser.add_argument('-o','--organization', action="store", dest="organization", default=deft_org )
parser.add_argument('-c','--comment', action="store", dest="comment", default=deft_comment)
parser.add_argument('-e','--email', action="store", dest="email", default=deft_email)

myscriptopts = parser.parse_args()

header = """\
%s
####################################################################################################
#   %s
#   %s
#   -%s %s %s
#   %s                         
####################################################################################################
""" % (myscriptopts.interpreter, myscriptopts.scriptname, myscriptopts.comment, myscriptopts.author,
       myscriptopts.organization, date_init, myscriptopts.email)

# write header to file
myscript = open(myscriptopts.scriptname, "w")
myscript.write(header)
myscript.close()

# since this is a script give it execute permission
myscriptfullpath = os.getcwd() + "/" + myscriptopts.scriptname
os.chmod(myscriptfullpath, 0o755)

print("Header for script \'%s\' successfully generated" % myscriptopts.scriptname)
