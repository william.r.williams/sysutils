#!/usr/bin/python

import os
import sys
import getopt
import re

# python function to make certain system cmds are present

# recursively look for files with a specific extension, check contents
def chk_file_cfg(location, ext, contents):
  for root, directories, filenames in os.walk(location):
    for filename in filenames:
      if os.path.join(root,filename).endswith(ext):
        open_Xfile = open(os.path.join(root,filename), "r")
        for line in open_Xfile.readlines():
          if re.match(contents, line):
            if not os.path.exists(line.replace(contents, '')):
              print ("\nERROR: In %s DPATH data file \n'%s' WAS NOT FOUND\n" % 
              (os.path.join(root,filename), line.replace(contents, '')))




# main
searchdir = "~"
chk_file_cfg(searchdir, ".txt", "DPATH:")
