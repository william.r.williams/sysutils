#!/usr/bin/python

import os
import sys
import getopt
import re
import logging
import time
#from sys import stderr

# chill out for a few minutes & let things stabilize
naptime = 5
os.system("sleep %d" % (naptime))

# OK, let's begin. Open the log file...
logfile = "u100.log"
logging.basicConfig(filename=logfile, level=logging.INFO)
numreboots = os.popen("grep 'executed at' %s |wc -l" % logfile).read().rstrip()
logging.info((" %s executed at %s following reboot number %s") % (sys.argv[0], time.strftime("%a %d%b%Y %H:%M:%S %Z"), numreboots))

# use lsusb to first determine if the device is still there
sangoma = os.popen("lsusb -d 10c4:8461").read().rstrip()

if sangoma:
    logging.info(" Sangoma U100 still being seen by lsusb utility...\n")
else:
    logging.error(" Sangoma U100 was not recognized by lsusb utility!\n")

#look at usb device in proc? sysfs??

#compare with what it should look like

#syslog

# look for errors dmesg output - first let's create a new file to diff with the old
dmesg_file = "dmesg_" + time.strftime("%Y%m%d%H%M%S")
d = open(dmesg_file, 'w')
dmesg_out = os.popen("dmesg").read()
d.write(dmesg_out)
d.close

# diff gets created

# search for any new usb errors and log them


# the new file we created becomes the previous
os.symlink(dmesg_file, "dmesg.last")

# if the below occur that's bad - usually has to do with the tnic's fxo device disappearing
#  wanpipe: No AFT/S514/S508 cards found, unloading modules!
#  sdla-usb-bus:3-1: USB device is disconnected!
#  usbcore: deregistering interface driver sdlausb
#  drivers/hid/usbhid/hid-core.c: can't reset device, 0000:00:1d.7-5.4/input3, status -71

# if alot of these types of errors are occurring then something can be wrong
usb 2-5: USB disconnect, address 2

# these are more rare but can also be indicative that something's wrong
#  hub 2-0:1.0: connect-debounce failed, port 5 disabled
#  hub 3-0:1.0: port 2 disabled by hub (EMI?), re-enabling
#  hub 3-0:1.0: unable to enumerate USB device on port 1


#usb package capture

# reboot
